#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <unordered_map>

class ConfigLoader
{
#pragma region public functions
public:
	ConfigLoader();
	~ConfigLoader();
	void OpenFile(const char* file);
	void CloseFile();
	#pragma region Search Functions.
	std::string SearchFileForSingleLine(const char* file, std::string whatToFind);
	std::string SearchFileForStruct(const char* file, std::string whatToFind);
	#pragma endregion
	#pragma region LoadNmuberFromFile() overrides 
	void LoadNumberFromFile(int& value, const char* file, std::string whatToFind);
	void LoadNumberFromFile(float& value, const char* file, std::string whatToFind);
	void LoadNumberFromFile(double& value, const char* file, std::string whatToFind);
	void LoadNumberFromFile(long& value, const char* file, std::string whatToFind);
	#pragma endregion
	void LoadFromStructToMap(std::unordered_map<std::string, std::string> &mapToLoadDataInto, const char* file, std::string whatToFind);
	void SaveToFile();
	void getNumberFromMap(std::unordered_map<std::string, std::string> map, std::string key, int &value);
	void getNumberFromMap(std::unordered_map<std::string, std::string> map, std::string key, float &value);
	void getNumberFromMap(std::unordered_map<std::string, std::string> map, std::string key, double &value);
	void getNumberFromMap(std::unordered_map<std::string, std::string> map, std::string key, long &value);
	void getStringFromMap(std::unordered_map<std::string, std::string> map, std::string key, std::string &value);
	void getBoolFromMap(std::unordered_map<std::string, std::string> map, std::string key, bool &value);
#pragma endregion
#pragma region Private Functions
private:
	const void HasOpenedSuccesfully();
	void getKey(std::string &stringToGetKeyFrom, std::string &key, unsigned pos);
	void getValue(std::string &stringToGetValueFrom, std::string &value, unsigned &pos);
	std::string extractDataFromStructString(std::string &structData);
#pragma endregion
#pragma region private data 
private:
	std::fstream currentFile;
#pragma endregion
};