#include "ConfigLoader.h"

#pragma region Constructor & Deconstructor
ConfigLoader::ConfigLoader()
{
}

ConfigLoader::~ConfigLoader()
{
}
#pragma endregion

#pragma region Functions For Open & Closing a File
/*
* Used to check if the file has opened succesfully. 
* If it has an appropriate message is displayed.
* If it cannot be opened then, again, an appropriate error message is displayed
*/
const void ConfigLoader::HasOpenedSuccesfully()
{
	if (currentFile.is_open())
	{
		std::cout << "File opened Succesfully" << std::endl;
	}
	else{ std::cout << "File Could Not Be Opened" << std::endl; }
}

/*
* Opens a file giving write and read access.
* After calling the function to open the file it checks if the file
* has been opened succesfully.
*
* @param const char* file - This is the file that is to be opened. 
*/
void ConfigLoader::OpenFile(const char* file)
{
	currentFile.open(file, std::ios::in | std::ios::out);
	HasOpenedSuccesfully();
}

/*
* Closes a file.
*/
void ConfigLoader::CloseFile()
{
	currentFile.close();
}
#pragma endregion

#pragma region File Maipulation(Loading, Saving etc.)

	#pragma region Searching File For Single Line & for Structures.
/*
* This function is used to search a file in order to find a value that
* will then be used to load some value in. It sets the file to the start after each search
* In order to make sure it is never at eof when searching. 
*
* @param const char* file - This is the file path for the file that is to be searched. 
* @param std::string whatToFind - This is what is being searched for.
* @return std::string line - The function returns the line that the string was found on.
*/
std::string ConfigLoader::SearchFileForSingleLine(const char* file, std::string whatToFind)
{
	if (currentFile.is_open() == false){ OpenFile(file); }
	size_t pos;
	std::string line;
	while (currentFile.good())
	{
		getline(currentFile, line);
		pos = line.find(whatToFind);
		if (pos != std::string::npos)
		{
			std::cout << "Found! " << whatToFind << std::endl;
			break;
		}
	}
	currentFile.clear();
	currentFile.seekg(0, std::ios::beg);
	return line;
}

/*
* This function finds a structure and loads all the data into a string when it is found. 
* It does this by using a while loop to find the indentifier while not at eof/file is still good.
* Once it has been found then it prints out a "Found!" message and then the identifier.
* It then sets a bool found to true, this then allows another while loop to be entered 
* which does while "}" is not found(or when endPos still equals std::string npos)
* which is what std::string:find returns if it is not found. It uses a temp string inside the loop and
* appends structToLoadFromFile with the next line until "}" is found. When "}" is found break is called
* to break out of the original loop. 
*
* @param const char* file - This is the file path for the file that is to be searched.
* @param std::string whatToFind - This is what is being searched for.
* @return std::string structToLoadFromFile - The function returns the block of text from where the identifier
*											 was found until the first "}" that it finds.
*/
std::string ConfigLoader::SearchFileForStruct(const char* file, std::string whatToFind)
{
	size_t startPos;
	size_t endPos = std::string::npos; 
	std::string structToLoadFromFile;
	std::string temp;
	bool found = false;

	if (currentFile.is_open() == false){ OpenFile(file); }

	while (currentFile.good())
	{
		getline(currentFile, structToLoadFromFile);
		startPos = structToLoadFromFile.find(whatToFind);
		if (startPos != std::string::npos)
		{
			std::cout << "Found! " << whatToFind << std::endl;
			found = true;
		}
		if (found == true)
		{
			while (endPos == std::string::npos)
			{
				getline(currentFile, temp);
				structToLoadFromFile += temp; 
				endPos = structToLoadFromFile.find("}");
			}
			break;
		}
	}
	currentFile.clear();
	currentFile.seekg(0, std::ios::beg);
	return structToLoadFromFile;
}
	#pragma endregion

	#pragma region All LoadNumberFromFile() functions. 
/*
* This function is used to load an int in from a text file. It does this by
* calling the SearchFile function above, takes the line that the search function returns
* and uses it to get the value assigned to that string.
* @param int& value - This is the value that has been loaded in.
* @param const char* file - This is the file path for the file that is to be searched.
* @param std::string whatToFind - This is what is being searched for.
*/
void ConfigLoader::LoadNumberFromFile(int& value, const char* file, std::string whatToFind)
{
	std::string lineContainingValue;
	std::string valueToLoad;
	lineContainingValue = SearchFileForSingleLine(file, whatToFind);
	unsigned pos = (unsigned)lineContainingValue.find(":");
	valueToLoad = lineContainingValue.substr(pos + 1);
	if (valueToLoad != "")
	{ 
		value = std::stoi(valueToLoad); 
	}
	else{ std::cout << "No value could be found to be loaded" << std::endl; }
}

/*
* Refer to first function comments, instead of int though it is a float.
* @param float& value - This is the value that has been loaded in.
*/
void ConfigLoader::LoadNumberFromFile(float& value, const char* file, std::string whatToFind)
{
	std::string lineContainingValue;
	std::string valueToLoad;
	lineContainingValue = SearchFileForSingleLine(file, whatToFind);
	unsigned pos = (unsigned)lineContainingValue.find(":");
	valueToLoad = lineContainingValue.substr(pos + 1);
	if (valueToLoad != "")
	{ 
		value = std::stof(valueToLoad);
	}
	else{ std::cout << "No value could be found to be loaded" << std::endl; }
}

/*
* Refer to first function comments, instead of int though it is a double.
* @param double& value - This is the value that has been loaded in.
*/
void ConfigLoader::LoadNumberFromFile(double& value, const char* file, std::string whatToFind)
{
	std::string lineContainingValue;
	std::string valueToLoad;
	lineContainingValue = SearchFileForSingleLine(file, whatToFind);
	unsigned pos = (unsigned)lineContainingValue.find(":");
	valueToLoad = lineContainingValue.substr(pos + 1);
	if (valueToLoad != "")
	{
		value = std::stod(valueToLoad); 
	}
	else{ std::cout << "No value could be found to be loaded" << std::endl; }
}

/*
* Refer to first function comments, instead of int though it is a long.
* @param long& value - This is the value that has been loaded in.
*/
void ConfigLoader::LoadNumberFromFile(long& value, const char* file, std::string whatToFind)
{
	std::string lineContainingValue;
	std::string valueToLoad;
	lineContainingValue = SearchFileForSingleLine(file, whatToFind);
	unsigned pos = (unsigned)lineContainingValue.find(":");
	valueToLoad = lineContainingValue.substr(pos + 1);
	if (valueToLoad != "")
	{
		value = std::stol(valueToLoad); 
	}
	else{ std::cout << "No value could be found to be loaded" << std::endl; }
}
	#pragma endregion

/*
* Separates the stuct values from the identifier and the starting curly bracket.
* The end curly bracket is used to identify the end of the string in LoadFromStructToMap
* function. 
*
* @param std::string &structData - The full string of the struct including identifier 
*								   and curly brackets.
*/
std::string ConfigLoader::extractDataFromStructString(std::string &structData)
{
	std::string separateStructValues;
	unsigned startOfStruct = (unsigned)structData.find("{");
	unsigned endOfStruct = (unsigned)structData.find("}");
	separateStructValues = structData.substr(startOfStruct + 1, endOfStruct);
	return separateStructValues;
}

/*
* Takes in a string in which there is a key to a value and a value in. They should be seperated by ":".
* If they are then the key is seperated from the value.
* If there is no ":" then an appropriate error merssage is displayed.
* 
* @param std::string &stringToGetKeyFrom - This is the original string containing both key and value.
* @param std::string &key - The key.
* @param unsigned pos - The pos that the find is to begin at for the string. This guarantees it will find the next
						key instead of the previous. 
*/
void ConfigLoader::getKey(std::string &stringToGetKeyFrom, std::string &key, unsigned pos)
{
	size_t endPos = (unsigned)stringToGetKeyFrom.find(":", pos);
	if (endPos == std::string::npos)
	{
		std::cerr << "There was no : seperating the key and value" << std::endl;
	}
	else
	{
		key = stringToGetKeyFrom.substr(pos + 1, (endPos - pos) - 1);
	}
}

/*
* Takes a string that contains both a key and a value and then gets the value from it.
* The value must be seperated from the key by a ":" and ended with a ";".
* The function gets the string between these two characters and sets it to a string.
* If one of the two characters cannot be found then an error message is output.
* It also sets "pos" to the endpos + 1 of the last semi-colon that was found, meaning that
* in the next search it will start from this pos and read the next key and value.
*
* @param std::string &stringToGetValueFrom - contains both a key and a value
* @param std::string value - The value once seperated
* @param unsigned &pos - The pos of the semi-colon that was found in the find call.
*/
void ConfigLoader::getValue(std::string &stringToGetValueFrom, std::string &value, unsigned &pos)
{
	std::string valueToLoad;
	size_t startPos = (unsigned)stringToGetValueFrom.find(":", pos);
	size_t endPos = (unsigned)stringToGetValueFrom.find(";", pos + 1);
	pos = (unsigned)endPos;
	if (startPos == std::string::npos || endPos == std::string::npos)
	{
		std::cerr << "Either : or ; could not be found so the value could not be seperated." << std::endl;
	}
	else
	{
		value = stringToGetValueFrom.substr(startPos + 1, (endPos - startPos) - 1);
	}
}

/*
* This function is called to load data from a file - written as a struct in the file - into a map as strings. 
* Takes an unordered map, char and string in order to get the file, the correct data and to put it into a map. 
* it makes use of other functions to do this. The loop just checks if its at the end of the string as the end
* of the string is set by a "}" and if not loops through seperating the keys and values from the string - done
* by functions - and then puts them into a pair and inserts them into a map. 
* If no "}" can be found an error message is printed. 
*
* @param std::unordered_map<std::string, std::string> &mapToLoadDataInto - This is the map that is to get populated.
* @param const char* file - The file that is to be searched. 
* @param std::string whatToFind - The identifier to be searched for.
*/
void ConfigLoader::LoadFromStructToMap(std::unordered_map<std::string, std::string> &mapToLoadDataInto, const char* file, std::string whatToFind)
{
	std::pair<std::string, std::string> keyAndValue;
	std::string allStructData;
	std::string valueToLoad;
	std::string separateStructValues;
	std::string key;
	std::string value;

	unsigned semiColonPos;
	semiColonPos = 0;

	allStructData = SearchFileForStruct(file, whatToFind);
	separateStructValues = extractDataFromStructString(allStructData);
	size_t endOfStringPos = (unsigned)separateStructValues.find("}");
	
	if (endOfStringPos != std::string::npos)
	{
		for (unsigned i = 0; i != endOfStringPos; i = (semiColonPos + 1))
		{
			getKey(separateStructValues, key, semiColonPos);
			getValue(separateStructValues, value, semiColonPos);
			keyAndValue.first = key;
			keyAndValue.second = value;
			mapToLoadDataInto.insert(keyAndValue);
		}
	}
	else(std::cerr << "No end of string could be found in LoadFromStructToMap in ConfigLoader." << std::endl);
}

void ConfigLoader::SaveToFile()
{
}

#pragma endregion

#pragma region Searching Map

/*
* Iterates through the map to find the key that is passed in. If it is not found then "not found" is printed out.
* Otherwise, it takes the second value of the iterator(or key) and converts it to an int, making the value that
* is passed in by reference equal the int.
*
* @param std::unordered_map<std::string, std::string> map - The map that is searched.
* @param std::string key - The key that is to be searched for.
* @param int &value - The value to get out of the map. 
*/
void ConfigLoader::getNumberFromMap(std::unordered_map<std::string, std::string> map, std::string key, int &value)
{
	std::unordered_map<std::string, std::string>::const_iterator it = map.find(key);

	if (it == map.end())
	{
		std::cout << "not found";
	}
	else
	{
		value = std::stoi(it->second);
	}
}

/*
* Does the same as above expect with a float instead of int
*
* @param float &value - The value to get out of the map. 
*/
void ConfigLoader::getNumberFromMap(std::unordered_map<std::string, std::string> map, std::string key, float &value)
{
	std::unordered_map<std::string, std::string>::const_iterator it = map.find(key);

	if (it == map.end())
	{
		std::cout << "not found";
	}
	else
	{
		value = std::stof(it->second);
	}
}

/*
* Does the same as above expect with a double instead of float
*
* @param double &value - The value to get out of the map.
*/
void ConfigLoader::getNumberFromMap(std::unordered_map<std::string, std::string> map, std::string key, double &value)
{
	std::unordered_map<std::string, std::string>::const_iterator it = map.find(key);

	if (it == map.end())
	{
		std::cout << "not found";
	}
	else
	{
		value = std::stod(it->second);
	}
}

/*
* Does the same as above expect with a long instead of double
*
* @param long &value - The value to get out of the map.
*/
void ConfigLoader::getNumberFromMap(std::unordered_map<std::string, std::string> map, std::string key, long &value)
{
	std::unordered_map<std::string, std::string>::const_iterator it = map.find(key);

	if (it == map.end())
	{
		std::cout << "not found";
	}
	else
	{
		value = std::stol(it->second);
	}
}

/*
* Does the same as above expect with a string instead of long. Only difference is, no conversion from string needed. 
*
* @param string &value - The value to get out of the map.
*/
void ConfigLoader::getStringFromMap(std::unordered_map<std::string, std::string> map, std::string key, std::string &value)
{
	std::unordered_map<std::string, std::string>::const_iterator it = map.find(key);

	if (it == map.end())
	{
		std::cout << "not found";
	}
	else
	{
		value = it->second;
	}
}

/*
* Does similar to the others, except it checks for the words "true" or "false" to decide what to set the value to.
* If its true it will set the bool to true and if its false it wil set it to true. If its neither a suitable
* message is printed to the programmer. 
*
* @param bool &value - The value to get out of the map.
*/
void ConfigLoader::getBoolFromMap(std::unordered_map<std::string, std::string> map, std::string key, bool &value)
{
	std::unordered_map<std::string, std::string>::const_iterator it = map.find(key);

	if (it == map.end())
	{
		std::cout << "not found";
	}
	else
	{
		if(it->second == "true")
		{
			value = true; 
		}
		else if(it->second == "false")
		{
			value = false; 
		}
		else std::cerr << "Neither a true or false value found in the map, therefore the key passed does not link to a bool." << std::endl; 
	}
}

#pragma endregion