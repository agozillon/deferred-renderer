#include "GLSLShaderProgram.h"

#include <iostream>
#include <string>

#ifdef _WIN32
#include <glm/gtc/type_ptr.hpp>
#elif __APPLE__
#include "type_ptr.hpp"
#endif


GLSLShaderProgram::~GLSLShaderProgram()
{
	glDeleteProgram(m_shaderProgramID); // delete the shader program from the GPU
}

GLuint GLSLShaderProgram::getUniformLocation(const char* uniformName) const
{
	return glGetUniformLocation(m_shaderProgramID, uniformName);
}

// sets a light structure named light in the GLSLShaderProgram, won't work
// for any light structure named something else in the GLSLShaderProgram
void GLSLShaderProgram::setLight(lightStruct light) const 
{
	// pass in light data to ShaderProgram
	setUniform4fv("light.diffuse", 1, light.diffuse);
	setUniform4fv("light.specular", 1, light.specular);
}

// takes in a lightstructure array name I.E lights[0], lights[1] and the number of the
// lights in the array and then loops through them all adding .position at the end
// and then finding the uniform and passing it to the GLSLShaderProgram!
void GLSLShaderProgram::setMultipleLightPos(const glm::vec4 lightPos[], int numberOf, const char* uniformNames[]) const
{
	std::string current;

	for (int i = 0; i < numberOf; i++)
	{
		current = uniformNames[i];
		current += ".position";
		setUniform4fv(current.c_str(), 1, glm::value_ptr(lightPos[i]));
	}

}

// works the same as the lightPos except it sends full light structures!
void GLSLShaderProgram::setLights(const lightStruct light[], int numberOf, const char* uniformNames[]) const
{
	std::string current;

	for (int i = 0; i < numberOf; i++)
	{
		// pass in light data to GLSLShaderProgram
		current = uniformNames[i];
		current += ".diffuse";
		setUniform4fv(current.c_str(), 1, light[i].diffuse);

		current = uniformNames[i];
		current += ".specular";
		setUniform4fv(current.c_str(), 1, light[i].specular);

	}

}

// sets a material structure named material in the GLSLShaderProgram, won't work
// for any material structure named something else in the GLSLShaderProgram
void GLSLShaderProgram::setMaterial(materialStruct material) const 
{
	// pass in material data to GLSLShaderProgram 
	setUniform4fv("material.ambient", 1, material.ambient);
	setUniform4fv("material.diffuse", 1, material.diffuse);
	setUniform4fv("material.specular", 1, material.specular);
	setUniformi("material.shininess", material.shininess);
}

// sends a single float to the shader program
void GLSLShaderProgram::setUniform1f(const char* uniformName, GLfloat data) const
{
	glUniform1f(getUniformLocation(uniformName), data);
}

// sends a vec2 to the shader program
void GLSLShaderProgram::setUniform2fv(const char* uniformName, int count, const GLfloat* data) const
{
	glUniform2fv(getUniformLocation(uniformName), count, data);
}

// sends a vec3 to the shader program
void GLSLShaderProgram::setUniform3fv(const char* uniformName, int count, const GLfloat* data) const
{
	glUniform3fv(getUniformLocation(uniformName), count, data);
}

// sends a vec4 to the shader program
void GLSLShaderProgram::setUniform4fv(const char* uniformName, int count, const GLfloat* value) const
{
	glUniform4fv(getUniformLocation(uniformName), count, value);
}

// sends a 3x3 matrix to the shader program
void GLSLShaderProgram::setUniformMatrix3fv(const char* uniformName, int count, GLboolean transpose, const GLfloat* data) const
{
	glUniformMatrix3fv(getUniformLocation(uniformName), count, transpose, data);
}

// sends a 4x4 matrix to the shader program
void GLSLShaderProgram::setUniformMatrix4fv(const char* uniformName, int count, GLboolean transpose, const GLfloat* data) const
{
	glUniformMatrix4fv(getUniformLocation(uniformName), count, transpose, data);
}

// get a uniform name and send an integer to the GLSLShaderProgram
void GLSLShaderProgram::setUniformi(const char* uniformName, GLuint data) const
{
	glUniform1i(getUniformLocation(uniformName), data);
}

// adds an attribute onto the map
void GLSLShaderProgram::addAttribute(GLuint index, const char* attribName)
{
	m_attribList[index] = attribName;
}

// sets up the default input attributes for the map that
// most shaders use, position, colour(might remove), normal, tex coords
void GLSLShaderProgram::addDefaultAttributes()
{
	m_attribList[0] = "in_Position";
	m_attribList[1] = "in_Color";
	m_attribList[2] = "in_Normal";
	m_attribList[3] = "in_TexCoord";
}

// clears the current attribute map (doesn't do anything to the shader program after compilation
// just clears the map holding the data)
void GLSLShaderProgram::clearAttributes()
{
	m_attribList.clear();
}

// adds an output for the shader program (fragment stage output, i.e colour)
void GLSLShaderProgram::addFragOutput(GLuint index, const char* outputName)
{
	m_fragOutputList[index] = outputName;
}

// adds the defualt frag output, colour (out_Color)
void GLSLShaderProgram::addDefaultOutput()
{
	m_fragOutputList[0] = "out_Color";
}

// clears the fragment output map (doesn't do anything to the shader program after compilation
// just clears the map holding the data)
void GLSLShaderProgram::clearFragOutput()
{
	m_fragOutputList.clear();
}

// attach a shader to the shader program, when compiled all of the attached shaders will make up the shader program
// I.E attach a phong.vert and a phong.frag shader to create a phong shader 
void GLSLShaderProgram::attachShader(GLuint shaderID)
{
	m_attachedShaders.push_back(shaderID);
}

// compiles the shader program, should only be called once
void GLSLShaderProgram::compileShaderProgram()
{
	m_shaderProgramID = glCreateProgram();

	// attaching all the various shader stages together to create the program
	for (size_t i = 0; i < m_attachedShaders.size(); ++i)
		glAttachShader(m_shaderProgramID, m_attachedShaders[i]);

	// loop through current attriblist and use for this shader link
	for (std::map<const GLuint, const char*>::iterator it = m_attribList.begin(); it != m_attribList.end(); ++it)
		glBindAttribLocation(m_shaderProgramID, it->first, it->second);
	
	// loop through current outputList and use for this shader link
	for (std::map<const GLuint, const char*>::iterator it = m_fragOutputList.begin(); it != m_fragOutputList.end(); ++it)
    {
        glBindFragDataLocation(m_shaderProgramID, it->first, it->second);
    }
	
	// link the shader stages to make the program and then use it 
	glLinkProgram(m_shaderProgramID);
	glUseProgram(m_shaderProgramID);

    GLint maxLength = 0;
    glGetProgramiv(m_shaderProgramID, GL_INFO_LOG_LENGTH, &maxLength);
	
    //The maxLength includes the NULL character
    std::vector<char> errorLog(256);
    glGetProgramInfoLog(m_shaderProgramID, 512, &maxLength, &errorLog[0]);
	
	for(std::vector<char>::iterator it = errorLog.begin(); it != errorLog.end(); ++it) {
        std::cout << *it;
    }
	
	bool glerror = false; while (glerror == false) { GLenum error = glGetError(); std::cout << error << std::endl; if (error == GL_NO_ERROR) glerror = true; }
    
    
	// detaching all the various shader stages as there no longer needed after linkng
	// also allows for shader deletion later, shaders don't delete even when its called
	// unless they're detached from all shader programs or the shader program there
	// attached to deletes!
	for (size_t i = 0; i < m_attachedShaders.size(); ++i)
		glDetachShader(m_shaderProgramID, m_attachedShaders[i]);

	// data no longer required
	m_attachedShaders.clear();
	m_attribList.clear();
	m_fragOutputList.clear();
}
