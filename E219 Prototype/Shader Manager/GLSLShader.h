#ifndef GLSLSHADER_H
#define GLSLSHADER_H
#include <GL\glew.h>
#include <string>

// GL 3.3 shader types, GL_COMPUTE/GL_TESS Eval and Control are 4.0+
enum ShaderType{
	VERTEX,
	FRAGMENT,
	GEOMETRY
};

// This class is specifically for GLSL Shader comipilation and shader ID storage
// Note: GLSLShaderProgram and Shader are very distinct things, Shader is one section of a GLSLShaderProgram
// I.E a vertex shader, fragment shader, geometry shader etc. The GLSLShaderProgram is a compiled 
// combination of several shaders that make a program!
class GLSLShader
{
public:
	GLSLShader(const char* fileName, ShaderType type); // The constructor is all you need to call to create and compile a shader
	~GLSLShader(); // deletes the shader from the GPU's memory
	GLuint getShaderID() const { return m_shaderID; } // passes the shader ID back
	
private:
	std::string loadShaderFile(const char* fileName) const; // loads in a file, in this case shader files
	void initShader(std::string shaderSource); // passes source code to the GPU (referenced by the shader ID) then compiles the shader

	GLuint m_shaderID;
};

#endif