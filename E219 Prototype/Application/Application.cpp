#include "Application.h"
#include <ctime>

#pragma region Class Constructors & Destructors
/*
 * Constructs the Application class. Sets m_running to true and instantiates m_serviceManager.
 */
Application::Application()
{
	std::cout << "Constructing Application..." << std::endl << std::endl;	
	
	m_running = true;

	m_serviceManager = std::shared_ptr<ServiceManager>(new ServiceManager());
	std::cout << "Constructed Application successfully." << std::endl << std::endl;
}

/*
 * Destructs the Application class.
 */
Application::~Application()
{
	std::cout << "Destructed Application." << std::endl << std::endl;
}

#pragma endregion


#pragma region General Public Methods
/*
 * Initialises member variables of the Application class.
 */
void Application::init()
{
	/* Create, initialise and register ScreenManager with ServiceManager */
	m_screenManager = std::shared_ptr<SDLScreenManager>(new SDLScreenManager(m_serviceManager));
	m_serviceManager->registerService(m_screenManager);
	m_screenManager->createWindow("E219 Prototype", 1280, 720);
	m_screenManager->init();
	/* Register new TimerManager with ServiceManager */
	m_serviceManager->registerService(std::shared_ptr<TimerManager>(new TimerManager()));
	std::shared_ptr<TimerManager> test = m_serviceManager->getTimerService();

	/* Register new SDLInput with ServiceManager */
	m_inputService = std::shared_ptr<SDLInput>(new SDLInput());
	m_serviceManager->registerService(m_inputService);

    
	/* Register new FMODAudio with ServiceManager */
	std::shared_ptr<FMODAudio> audioService = std::shared_ptr<FMODAudio>(new FMODAudio());
	m_serviceManager->registerService(audioService);
}

/*
 * Application loop. Loop will run whilst m_running is set to true, and will stop once m_running has been set to false.
 * Checks for SDL events and updates m_screenManager.
 *
 * @return m_running - Returns a boolean variable which signifys if the application loop is currently running or not.
 */
bool Application::run()
{
	clock_t beforeTime, afterTime;
	while (m_running == true)
	{
		m_inputService->update();

		while (SDL_PollEvent(&m_SDLEvent))
		{
			switch (m_SDLEvent.type)
			{
				case SDL_WINDOWEVENT:
					switch (m_SDLEvent.window.event)
					{
						case SDL_WINDOWEVENT_CLOSE:
							m_running = false;
							break;
					}
					break;
				case SDL_QUIT:
					m_running = false;
					break;
			}
		}

		beforeTime = clock();
		m_screenManager->update();
		m_screenManager->render();
		afterTime = clock();
		std::cout << "Full Timer: " << (double)(((double)afterTime - (double)beforeTime) / CLOCKS_PER_SEC) << std::endl;

	}
	return m_running;
}

#pragma endregion