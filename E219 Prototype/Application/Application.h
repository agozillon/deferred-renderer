#pragma once

#include "../Service Manager/ServiceManager.h"
#include "../Screens/SDLScreenManager.h"
#include "../Input/SDLInput.h"
#include "../Audio/FMODAudio.h"
#include "../TimerManager/TimerManager.h"
#include "../Entities/EntityManager.h"

class Application
{
public:
	/* Class Constructors & Destructors*/
	Application();
	~Application();

public:
	/* General Public Methods */
	void init();
	bool run();

private:
	bool m_running;
	SDL_Event m_SDLEvent;

	std::shared_ptr<ServiceManager> m_serviceManager;
	std::shared_ptr<SDLScreenManager> m_screenManager;
	std::shared_ptr<SDLInput> m_inputService;
};

