#ifndef CAMERA_H
#define CAMERA_H

#ifdef _WIN32
#include <glm/gtc/type_ptr.hpp>
#elif __APPLE__
#include "type_ptr.hpp"
#endif

using namespace glm;

// abstract base class for creating cameras specifys that camera classes that inherit require get/set functions, updates and moves
// this could be refined and changed for other camera types i.e not all cameras require a far perhaps. So could change it to virtual
// instead of pure virtual and then create an empty function. Any camera that inherits but doesn't implement it will then delegate 
// the call to the Camera interface.
class Camera
{

public:
	virtual ~Camera(){}									     // virtual inline destructor
	virtual vec3 getEye() const = 0;                   // virtual function that should be implemented to return the vec3 Eye value
	virtual vec3 getAt() const = 0;                    // virtual function that should be implemented to return the vec3 At value 
	virtual vec3 getUp() const = 0;					 // virtual function that should be implemented to return the vec3 Up value 
	virtual float getYaw() const = 0;					 // virtual function that should be implemented to return the cameras rotation
	virtual float getPitch() const = 0;				 // virtual function that should be implemented to return the cameras rotation	
	virtual mat4 getView() const = 0;					 // virtual function that should be implemented to return the view matrix
	virtual float getFOV() const = 0;				     // virtual function that should be implemented to return the cameras FOV
	virtual float getNear() const = 0;				 // virtual function that should be implemented to return the cameras near cutting distance
	virtual float getFar() const = 0;					 // virtual function that should be implemented to return the cameras far cutting distance
	virtual float getAspectRatio() const = 0;			 // virtual function that should be implemented to return the cameras aspect ratio
	virtual mat4 getProjection() const = 0;            // virtual function that should be implemented to return the cameras projection matrix

	virtual void updateEye(float x, float y, float z) = 0;	 // virtual function that should be implemented to update the Eye position
	virtual void updateAt(float x, float y, float z) = 0;	 // virtual function that should be implemented to update the At position
	virtual void updateUp(float x, float y, float z) = 0;    // virtual function that should be implemented to update the Up position
	virtual void updateYaw(float rot) = 0;					 // virtual function that should be implemented to update the cameras rotation
	virtual void updatePitch(float rot) = 0;				 // virtual function that should be implemented to update the cameras rotation
	virtual void updateAspectRatio(float aspectRatio) = 0;   // virtual function that should be implemented to update the cameras aspect ratio
	

	// Can implement these, not required though will just call this blank function
	// Would want these for 3rd person/first person but perhaps not for a "2D Camera"
	virtual void move(float distRight, float distUp, float distForward){}	 // function used to move the camera left or right by a passed in distance
	
};

#endif