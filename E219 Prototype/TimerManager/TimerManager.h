#pragma once

#include <iostream>
#include <array>

#include "Timers.h"

class TimerManager
{
public:
	/* Class Constructors & Destructors */
	TimerManager();
	~TimerManager();

public:
	/* Timer Management */
	void createNewTimer(std::string timerName, float length);
	void setTimer(std::string timerName, float length);
	void editTimer(std::string timerName, float length);
	void pauseTimer(std::string timerName);
	bool checkTimer(std::string timerName);
	void stopTimer(std::string timerName);
	void startTimer(std::string timerName);
	void globalPause();
	void globalUnPause();
	void pause(std::string timerName);
	void unPause(std::string timerName);
	void globalStopTimer();
	void updateTimers();
	float getTimeLeft(std::string timerName);

private:
	/* Timer Manager Members*/
	std::array<Timers, 20 > m_timerList;

};

