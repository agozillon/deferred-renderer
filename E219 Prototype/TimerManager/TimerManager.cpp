#include"TimerManager.h"


#pragma region Class Constructors & Destructors 
/*
* Constructs the TimerManager class.
*/

TimerManager::TimerManager()
{
	std::cout << "TimerManager Constructed Successfully" << std::endl;
}

/*
* Destructs the TimerManager class.
*/

TimerManager::~TimerManager()
{
	std::cout << "TimerManager Destructed Successfully" << std::endl;
}

#pragma endregion


#pragma region TimerManagement

/*
* allocates a timer to be used from the list of unused timers. If there are no timers available the program will exit and the console will tell you to increase the timer pool
*/

void TimerManager::createNewTimer(std::string timerName, float length)
{
	bool timerAllocated = false;
	for (unsigned int i = 0; i < m_timerList.size(); ++i)
	{
		if (m_timerList[i].checkUse() == false)
		{
			m_timerList[i].setName(timerName);
			m_timerList[i].setTime(length);
			m_timerList[i].setInUse();
			std::cout << "Allocated Timer: " << i << std::endl;
			timerAllocated = true;
			break;
		}
	}
	if (timerAllocated == false)
	{
		std::cout << "Your Timer Pool is not large enough please increase the array size!" << std::endl;
	}
}

/*
* Sets new time for timer
*/

void TimerManager::setTimer(std::string timerName, float length)
{
	for (unsigned int i = 0; i < m_timerList.size(); ++i)
	{
		if (m_timerList[i].checkName(timerName) == true)
		{
			m_timerList[i].setTime(length);
			break;
		}
		std::cout << "Could not find timer: " << timerName.c_str() << std::endl;
	}
}

/*
* Add more or subtract time from timer
*/

void TimerManager::editTimer(std::string timerName, float length)
{
	for (unsigned int i = 0; i < m_timerList.size(); ++i)
	{
		if (m_timerList[i].checkName(timerName) == true)
		{
			m_timerList[i].editTimer(length);
			break;
		}
		std::cout << "Could not find timer: " << timerName.c_str() << std::endl;
	}
}

/*
* Pauses the specified timer
*/

void TimerManager::pauseTimer(std::string timerName)
{
	for (unsigned int i = 0; i < m_timerList.size(); ++i)
	{
		if (m_timerList[i].checkName(timerName) == true)
		{
			m_timerList[i].pause();
			break;
		}
		std::cout << "Could not find timer: " << timerName.c_str() << std::endl;
	}
}

/*
* Checks if timer is finished or reached its goal - true for reached, false for not reached yet
*/

bool TimerManager::checkTimer(std::string timerName)
{
	for (unsigned int i = 0; i < m_timerList.size(); ++i)
	{
		if (m_timerList[i].checkName(timerName) == true)
		{
			if (m_timerList[i].checkTime() == true)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	std::cout << "Could not find timer: " << timerName.c_str() << std::endl;
	return false;
}

/*
* Stops the timer then places it in the recycle pool
*/
void TimerManager::stopTimer(std::string timerName)
{
	for (unsigned int i = 0; i < m_timerList.size(); ++i)
	{
		if (m_timerList[i].checkName(timerName) == true)
		{
			m_timerList[i].stop();
			break;
		}
		std::cout << "Could not find timer: " << timerName.c_str() << std::endl;
	}
}

/*
* StartTimer
*/

void TimerManager::startTimer(std::string timerName)
{
	for (unsigned int i = 0; i < m_timerList.size(); ++i)
	{
		if (m_timerList[i].checkName(timerName) == true)
		{
			m_timerList[i].start();
			break;
		}
		std::cout << "Could not find timer: " << timerName.c_str() << std::endl;
	}
}

/*
* Pauses all timers in use
*/

void TimerManager::globalPause()
{
	for (unsigned int i = 0; i < m_timerList.size(); ++i)
	{
		m_timerList[i].pause();
	}
}

/*
* Unpause all timers in use
*/

void TimerManager::globalUnPause()
{
	for (unsigned int i = 0; i < m_timerList.size(); ++i)
	{
		m_timerList[i].unPause();
	}
}

/*
* Pause specific timer in use
*/

void TimerManager::pause(std::string timerName)
{
	for (unsigned int i = 0; i < m_timerList.size(); ++i)
	{
		if (m_timerList[i].checkName(timerName) == true)
		{
			m_timerList[i].pause();
			break;
		}
		std::cout << "Could not find timer: " << timerName.c_str() << std::endl;
	}
}

/*
* Unpause specific timer in use
*/

void TimerManager::unPause(std::string timerName)
{
	for (unsigned int i = 0; i < m_timerList.size(); ++i)
	{
		if (m_timerList[i].checkName(timerName) == true)
		{
			m_timerList[i].unPause();
			break;
		}
		std::cout << "Could not find timer: " << timerName.c_str() << std::endl;
	}
}

/*
* Stops all timers in use and puts them in the recycle pool to be used later: used for situations like game over or new game where they can start again
*/

void TimerManager::globalStopTimer()
{
	for (unsigned int i = 0; i < m_timerList.size(); ++i)
	{
		m_timerList[i].stop();
	}
}

/*
* Updates timers current time for all timers
*/


void TimerManager::updateTimers()
{
	for (unsigned int i = 0; i < m_timerList.size(); ++i)
	{
		m_timerList[i].updateTimer();
	}
}


/*
* Allows for manual returning of how much time is left for the specific timer
*/


float TimerManager::getTimeLeft(std::string timerName)
{
    float returnValue = 0.0f;
    
	for (unsigned int i = 0; i < m_timerList.size(); ++i)
	{
		if (m_timerList[i].checkName(timerName) == true)
		{
			returnValue = m_timerList[i].getTimeLeft();
		}
	}
    
    return returnValue;
}
#pragma endregion