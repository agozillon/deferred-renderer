#include "Timers.h"

#pragma region Class Constructors & Destructors 

/*
* Constructs the Timers class.
*/

Timers::Timers()
{
	m_paused = true;
	m_inUse = false;
	std::cout << "Timer Created Successfully" << std::endl;
}


/*
* Destructs the Timers class.
*/

Timers::~Timers()
{
	std::cout << "Timer Deleted Successfully" << std::endl;
}

#pragma endregion

#pragma region Manages Timers Properties

/*
* Sets the goal time for the timer
*/

void Timers::setTime(float length)
{
	m_goalTime = length;
}

/*
* Adds or subtracts time from the timer
*/

void Timers::editTimer(float length)
{
	m_goalTime += length;
}

/*
* Checks if Timer has passed its goal time and returns true if it has else it turns false only if the timer is in use and not paused
*/

bool Timers::checkTime()
{
	if (m_paused == false && m_inUse == true)
	{
		std::cout << ((m_startTime/1000) + m_goalTime) - (m_currentTime/1000) << std::endl;
		if (((m_startTime/1000) + m_goalTime) <= (m_currentTime/1000))
		{
			std::cout << "Goal has been reached! Timer is Paused" << std::endl;
			m_paused = true;
			return true;
		}
		else
		{

			return false;
		}
	}
	else
	{
		std::cout << "Timer is pause or not in use" << std::endl;
		return false;
	}
}

/*
* Pauses Timer
*/

void Timers::pause()
{
	m_paused = true;
}

/*
* Starts Timer
*/

void Timers::start()
{
	m_paused = false;
	m_startTime = SDL_GetTicks();
	m_currentTime = m_startTime;
}

/*
* Stops Timer
*/

void Timers::stop()
{
	m_inUse = false;
	m_paused = true;
	m_currentTime = 0.0f;
	m_goalTime = 0.0f;
	m_startTime = 0.0f;
}

/*
* UnPauses Timer
*/

void Timers::unPause()
{
	m_paused = false;
}

/*
* Updates Timer if it is running
*/

void Timers::updateTimer()
{
	if (m_inUse == true && m_paused == false)
	{
		m_currentTime = SDL_GetTicks();
	}
}


/*
* Checks if timer is in use
*/

bool Timers::checkUse()
{
	if (m_inUse == true)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*
* sets name for timer
*/

void Timers::setName(std::string name)
{
	m_name = name;
}


/*
* sets it to be in use and to be paused so it doesn't start automatically
*/

void Timers::setInUse()
{
	m_inUse = true;
	m_paused = true;
}


/*
* Checks name with intput returns true if matches else returns false1
*/

bool Timers::checkName(std:: string name)
{
	if (m_name == name)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*
* returns time left
*/


float Timers::getTimeLeft()
{
	return (((m_startTime / 1000) + m_goalTime) - m_currentTime/1000);
}

#pragma endregion

