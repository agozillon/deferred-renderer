#pragma once

#include <iostream>

#ifdef _WIN32
#include <SDL.h>
#elif __APPLE__
#include <SDL2/SDL.h>
#endif

class Timers
{

public:
	/* Class Constructors & Destructors */
	Timers();
	~Timers();

public:
	/* Manages Timers' properties*/
	void setTime(float length);
	void editTimer(float length);
	bool checkTime();
	void pause();
	void start();
	void stop();
	void unPause();
	void updateTimer();
	bool checkUse();
	void setName(std::string name);
	void setInUse();
	bool checkName(std::string name);
	float getTimeLeft();

private:
	/* Timers' members */
	bool m_inUse, m_paused;
	std::string m_name;
	float m_goalTime, m_startTime, m_currentTime;

};