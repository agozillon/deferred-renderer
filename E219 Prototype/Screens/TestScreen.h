#pragma once

#include <iostream>
#include <memory>
#include <stack>
#include <random>
#include <iostream>

#ifdef _WIN32
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#elif __APPLE__
#include "type_ptr.hpp"
#include "matrix_transform.hpp"
#include <OpenGL/gl3.h>
#endif

#include "Screen.h"
#include "../Loaders/AssimpLoader.h"
#include "../Camera Manager/CameraManager.h"
#include "../Shader Manager/ShaderManager.h"
#include "../Service Manager/ServiceManager.h"
#include "../Input/Input.h"
#include "../Loaders/TextureLoader.h"
#include "../Renderer/Texture/Texture2D.h"
#include "../Renderer/Render Manager/RenderManager.h"
#include "../Entities/EntityManager.h"

class TestScreen : public Screen
{
public:
	/* Class Constructors & Destructors */
	TestScreen(std::shared_ptr<ServiceManager> serviceManager, unsigned int windowWidth, unsigned int windowHeight);
	virtual ~TestScreen();

public:
	/* General Public Methods */
	virtual void init();
	virtual void update();
	virtual void render();

private:
	
	std::unique_ptr<AssimpLoader> m_loader;
	std::shared_ptr<CameraManager> m_cameras;
	std::shared_ptr<ShaderManager> m_shaders;
	std::shared_ptr<ServiceManager> m_serviceManager;
	std::unique_ptr<TextureLoader> m_textureLoader;
	std::unique_ptr<RenderManager> m_renderManager;

	unsigned int m_windowWidth;
	unsigned int m_windowHeight;

	// cube model mesh
	GLuint m_modelVAO;
	GLint m_indexCount;

	const char* m_test;

	// light mesh
	GLuint m_lightVAO;
	GLint m_lightIndexCount;

	// light count
	int m_lightCount;
	float m_rotationIncrement;
	
	std::mt19937 mtrng;
	std::shared_ptr<Texture2D> m_texture; // texture

	std::shared_ptr<EntityManager> m_entityManager; 

};

