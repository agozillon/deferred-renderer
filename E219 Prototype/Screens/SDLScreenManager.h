#pragma once

#include <iostream>
#include <unordered_map>
#include <vector>

#ifdef _WIN32
#include <SDL.h>
#include <GL/glew.h>
#elif __APPLE__
#include <SDL2/SDL.h>
#include <OpenGL/gl3.h>

#endif

#include "ScreenManager.h"
#include "Screen.h"
#include "TestScreen.h"
#include "../Service Manager/ServiceManager.h"

class SDLScreenManager : public ScreenManager
{
	/* Class Constructors & Destructors */
public:
	SDLScreenManager(std::shared_ptr<ServiceManager> serviceManager);
	virtual ~SDLScreenManager();

	/* General Public Methods */
public:
	virtual void init();
	virtual void update();
	virtual void render();

	/* Screen Management */
public:
	virtual void setCurrentScreen(std::string window);

private:
	std::unordered_map<std::string, std::shared_ptr<Screen>> m_screenMap;

	/* Window Management */
public:
	virtual void createWindow(std::string title, unsigned int width, unsigned int height);
	virtual void setWindowTitle(std::string title);
	virtual void setWindowSize(int width, int height);
	virtual void setWindowWidth(int width);
	virtual void setWindowHeight(int height);
	virtual void minimiseWindow();
	virtual void restoreWindow();
	virtual void maximiseWindow();
	virtual void setFullScreenMode();
	virtual void setWindowedMode();

private:
	SDL_GLContext m_GLContext;
	SDL_Window* m_window;
	unsigned int m_displayDevice;

	std::string m_windowTitle;
	unsigned int m_windowWidth;
	unsigned int m_windowHeight;
	
	SDL_DisplayMode m_displayMode;
	void setUpDisplayModeVector();
	std::vector<SDL_DisplayMode> m_displayModeVector;

private:
	std::shared_ptr<ServiceManager> m_serviceManager;
};

