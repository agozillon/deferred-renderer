#include "TestScreen.h"
#include <sstream>
#include <ctime>

#pragma region Class Constructors & Destructors

TestScreen::TestScreen(std::shared_ptr<ServiceManager> serviceManager, unsigned int windowWidth, unsigned int windowHeight)
{
	m_serviceManager = serviceManager;
	m_windowWidth = windowWidth;
	m_windowHeight = windowHeight;
}


TestScreen::~TestScreen()
{
}

#pragma endregion

#pragma region General Public Methods
/*
* Initialises the screen class.
*/
void TestScreen::init()
{
	m_lightCount = 1;
	m_rotationIncrement = 0;

	m_cameras = std::shared_ptr<CameraManager>(new CameraManager());
	m_cameras->addCamera("Demo", FIRSTPERSONCAMERA); // adding sets it up in a default location, with default aspect ratio etc.
	m_cameras->move(0, 0, -50);

	m_loader = std::unique_ptr<AssimpLoader>(new AssimpLoader());
	m_loader->load("Resources/Models/cube.obj", m_modelVAO, m_indexCount);
	m_loader->load("Resources/Models/lightsphere.obj", m_lightVAO, m_lightIndexCount);

	m_textureLoader = std::unique_ptr<TextureLoader>(new TextureLoader());

	std::shared_ptr<Texture2D> tmpTexture = m_textureLoader->loadTexture("Resources/Textures/studdedmetal.jpg");

	// Entity Manager
	m_entityManager = std::shared_ptr<EntityManager>(new EntityManager(100, 150));
	
	// Light 1
	m_entityManager->createEntity("LIGHT1");
	m_entityManager->addComponentToEntityName("LIGHT1", "POSITION3D");
	m_entityManager->addComponentToEntityName("LIGHT1", "SCALAR3D");
	m_entityManager->addComponentToEntityName("LIGHT1", "LIGHT");
	m_entityManager->addComponentToEntityName("LIGHT1", "MODEL");

	m_entityManager->getLightComponent("LIGHT1")->updateLightDiffuse(1.0f, 0.0f, 0.0f, 1.0f);
	m_entityManager->getLightComponent("LIGHT1")->updateLightSpecular(0.0f, 0.0f, 0.2f, 1.0f);
	m_entityManager->getModelComponent("LIGHT1")->updateMeshID(m_lightVAO);
	m_entityManager->getModelComponent("LIGHT1")->updateIndexCount(m_lightIndexCount);
	m_entityManager->getWorldPlacement3DComponent("LIGHT1", "POSITION3D")->setWorld3DPlacement(std::cos(18), std::sin(18), 2.0f);
	m_entityManager->getWorldPlacement3DComponent("LIGHT1", "SCALAR3D")->setWorld3DPlacement(2.5f, 2.5f, 5.0f);

	// wall
	m_entityManager->createEntity("MODEL1");
	m_entityManager->addComponentToEntityName("MODEL1", "ROTATION3D");
	m_entityManager->addComponentToEntityName("MODEL1", "POSITION3D");
	m_entityManager->addComponentToEntityName("MODEL1", "SCALAR3D");
	m_entityManager->addComponentToEntityName("MODEL1", "MODEL");

	m_entityManager->getModelComponent("MODEL1")->updateMeshID(m_modelVAO);
	m_entityManager->getModelComponent("MODEL1")->updateIndexCount(m_indexCount);
	m_entityManager->getModelComponent("MODEL1")->updateTextureID(tmpTexture);

	m_entityManager->getWorldPlacement3DComponent("MODEL1", "POSITION3D")->setWorld3DPlacement(0.0f, 0.0f, 0.0f);
	m_entityManager->getWorldPlacement3DComponent("MODEL1", "SCALAR3D")->setWorld3DPlacement(20.0f, 20.0f, 1.0f);
	m_entityManager->getWorldPlacement3DComponent("MODEL1", "ROTATION3D")->setWorld3DPlacement(0.0f, 0.0f, 0.0f);

	// Shader is a piece of a shader program, I.E vertex, fragment, Shader Program is the compiled and linked result
	// that we use to render geometry
	/////////////////////////////
	/////SEPERATE SHADER SETUP///
	/////////////////////////////
	m_shaders = std::shared_ptr<ShaderManager>(new ShaderManager());
	m_shaders->compileShader("null", "Resources/Shaders/Fragment/null.frag", FRAGMENT);
	m_shaders->compileShader("mvp", "Resources/Shaders/Vertex/MVP.vert", VERTEX);
	m_shaders->compileShader("textureVert", "Resources/Shaders/Vertex/textured.vert", VERTEX);
	m_shaders->compileShader("textureFrag", "Resources/Shaders/Fragment/textured.frag", FRAGMENT);
	m_shaders->compileShader("deferredFirstPassVert", "Resources/Shaders/Vertex/deferredfirstpass.vert", VERTEX);
	m_shaders->compileShader("deferredFirstPassFrag", "Resources/Shaders/Fragment/deferredfirstpass.frag", FRAGMENT);
	m_shaders->compileShader("deferredSecondPassFrag", "Resources/Shaders/Fragment/deferredsecondpass.frag", FRAGMENT);

	///////////////////////////
	///////TEXTURED PROGRAM////
	///////////////////////////
	m_shaders->createShaderProgram("texturedShader");
	m_shaders->addShaderProgramAttribute("texturedShader", 0, "in_Position");
	m_shaders->addShaderProgramAttribute("texturedShader", 1, "in_TexCoord");
	m_shaders->addShaderFragOutput("texturedShader", 0, "out_Color");
	m_shaders->attachShaderToProgram("texturedShader", "textureVert");
	m_shaders->attachShaderToProgram("texturedShader", "textureFrag");
	m_shaders->compileShaderProgram("texturedShader");

	
	m_renderManager = std::unique_ptr<RenderManager>(new RenderManager(m_windowWidth, m_windowHeight, m_shaders, m_cameras));
	
	// All the shaders have been created and detached from programs after linkage, time to clean up some GPU space
	m_shaders->deleteShaders();

	mtrng.seed(1);

	//Test code for GL errors, useful
	bool glerror = false;
	while (glerror == false)
	{
		GLenum error = glGetError();

		std::cout << error << std::endl;

		if (error == GL_NO_ERROR)
			glerror = true;
	}
}

/*
* Used to update the screen.
*/
void TestScreen::update()
{
	clock_t beforeTime = clock();
	static std::shared_ptr<Input> inputService = m_serviceManager->getInputService();

	// basically all of daniels move stuff combined into a move function, put 0 in the two others values and whatever move 
	// value you want in the other spot. Combinations will get you Diagonal movement etc. 
	if (inputService->getKeyState("A"))
		m_cameras->move(-1, 0, 0);

	if (inputService->getKeyState("D"))
		m_cameras->move(1, 0, 0);

	if (inputService->getKeyState("W"))
		m_cameras->move(0, 0, 1);

	if (inputService->getKeyState("S"))
		m_cameras->move(0, 0, -1);

	if (inputService->getKeyState("R"))
		m_cameras->move(0, 1, 0);

	if (inputService->getKeyState("F"))
		m_cameras->move(0, -1, 0);

	// rotate the camera left and right
	if (inputService->getKeyState("LEFT"))
		m_cameras->updateYaw(-1);

	if (inputService->getKeyState("RIGHT"))
		m_cameras->updateYaw(1);

	// rotate the camera up and down
	if (inputService->getKeyState("UP"))
		m_cameras->updatePitch(1);

	if (inputService->getKeyState("DOWN"))
		m_cameras->updatePitch(-1);
	
	// Adds a light entity to the demo
	if (inputService->getKeyState("SPACE") && m_lightCount <= 65)
	{
		m_lightCount++;

		// NOTE (to self, and others): aware this is going to cause a memory leak in the current entity manager, entity manager needs updated to use strings over chars
		// or to use shared_ptrs. Because as is theres a scope issue if you try define things on the fly (without the new it'd fall out of scope and the entity would lose
		// its entity name, causing a host of issues).
		char* str = new char[8]; 
		strcpy(str, "LIGHT");
		m_entityManager->createEntity(strcat(str, std::to_string(m_lightCount).c_str()));
		m_entityManager->addComponentToEntityName(str, "POSITION3D");
		m_entityManager->addComponentToEntityName(str, "SCALAR3D");
		m_entityManager->addComponentToEntityName(str, "LIGHT");
		m_entityManager->addComponentToEntityName(str, "MODEL");
		 
		m_entityManager->getLightComponent(str)->updateLightDiffuse((float)mtrng() / (float)mtrng.max(), (float)mtrng() / (float)mtrng.max(), (float)mtrng() / (float)mtrng.max(), 1.0f);
		m_entityManager->getLightComponent(str)->updateLightSpecular((float)mtrng() / (float)mtrng.max(), (float)mtrng() / (float)mtrng.max(), (float)mtrng() / (float)mtrng.max(), 1.0f);
		m_entityManager->getModelComponent(str)->updateMeshID(m_lightVAO);
		m_entityManager->getModelComponent(str)->updateIndexCount(m_lightIndexCount);
		m_entityManager->getWorldPlacement3DComponent(str, "POSITION3D")->setWorld3DPlacement(0.0f, 0.0f, 2.0f);
		
		if (m_lightCount > 11 && m_lightCount <= 22)
			m_entityManager->getWorldPlacement3DComponent(str, "SCALAR3D")->setWorld3DPlacement(2.0f, 2.0f, 5.0f);
		else if (m_lightCount > 22 && m_lightCount <= 33)
			m_entityManager->getWorldPlacement3DComponent(str, "SCALAR3D")->setWorld3DPlacement(1.0f, 1.0f, 5.0f);
		else if (m_lightCount > 33 && m_lightCount <= 44)
			m_entityManager->getWorldPlacement3DComponent(str, "SCALAR3D")->setWorld3DPlacement(0.5f, 0.5f, 5.0f);
		else if (m_lightCount > 44 && m_lightCount <= 55)
			m_entityManager->getWorldPlacement3DComponent(str, "SCALAR3D")->setWorld3DPlacement(0.2f, 0.2f, 5.0f);
		else if (m_lightCount > 55 && m_lightCount <= 66)
			m_entityManager->getWorldPlacement3DComponent(str, "SCALAR3D")->setWorld3DPlacement(0.1f, 0.1f, 5.0f);
		else
			m_entityManager->getWorldPlacement3DComponent(str, "SCALAR3D")->setWorld3DPlacement(2.5f, 2.5f, 5.0f);
	}
	
	// updates all of the light entities positions
	for (int i = 0; i < m_lightCount; ++i)
	{
		std::string entityName("LIGHT");
		entityName += std::to_string(i + 1);

		if (i >= 11 && i < 22)
			m_entityManager->getWorldPlacement3DComponent(entityName.c_str(), "POSITION3D")->setWorld3DPlacement(8 * std::cos(((i * 32) + m_rotationIncrement) * (3.14159265 / 180.0)), 8 * std::sin(((i * 32) + m_rotationIncrement) * (3.14159265 / 180.0)), 2.0f);
		else if (i >= 22 && i < 33)
			m_entityManager->getWorldPlacement3DComponent(entityName.c_str(), "POSITION3D")->setWorld3DPlacement(4.5 * std::cos(((i * 32) + m_rotationIncrement) * (3.14159265 / 180.0)), 4.5 * std::sin(((i * 32) + m_rotationIncrement) * (3.14159265 / 180.0)), 2.0f);
		else if (i >= 33 && i < 44)
			m_entityManager->getWorldPlacement3DComponent(entityName.c_str(), "POSITION3D")->setWorld3DPlacement(2 * std::cos(((i * 32) + m_rotationIncrement) * (3.14159265 / 180.0)), 2 * std::sin(((i * 32) + m_rotationIncrement) * (3.14159265 / 180.0)), 2.0f);
		else if (i >= 44 && i < 55)
			m_entityManager->getWorldPlacement3DComponent(entityName.c_str(), "POSITION3D")->setWorld3DPlacement(1 * std::cos(((i * 32) + m_rotationIncrement) * (3.14159265 / 180.0)), 1 * std::sin(((i * 32) + m_rotationIncrement) * (3.14159265 / 180.0)), 2.0f);
		else if (i >= 55 && i < 66)
			m_entityManager->getWorldPlacement3DComponent(entityName.c_str(), "POSITION3D")->setWorld3DPlacement(0.5 * std::cos(((i * 32) + m_rotationIncrement) * (3.14159265 / 180.0)), 0.5 * std::sin(((i * 32) + m_rotationIncrement) * (3.14159265 / 180.0)), 2.0f);
		else		
			m_entityManager->getWorldPlacement3DComponent(entityName.c_str(), "POSITION3D")->setWorld3DPlacement(15 * std::cos(((i * 32) + m_rotationIncrement) * (3.14159265 / 180.0)), 15 * std::sin(((i * 32) + m_rotationIncrement) * (3.14159265 / 180.0)), 2.0f);
	}
	

	m_rotationIncrement++;

	// doesn't have to be done as even if it increases it'll still go the full 360,
	// just y'know don't want it to exceed the maximum size of an integer if someone (unlikely)
	// leaves it on for too long...
	if (m_rotationIncrement >= 360) 
		m_rotationIncrement = 0;

	clock_t	afterTime = clock();
	std::cout << "Update Time: " << (double)(((double)afterTime - (double)beforeTime) / CLOCKS_PER_SEC) << std::endl;
}

/*
* Used to render the screen.
*/
void TestScreen::render()
{
	clock_t beforeTime = clock();
	m_renderManager->render(m_entityManager);
	clock_t	afterTime = clock();
	std::cout << "Render Time: " << (double)(((double)afterTime - (double)beforeTime) / CLOCKS_PER_SEC) << std::endl;
}

#pragma endregion

