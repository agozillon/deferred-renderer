#pragma once

#include "Application/Application.h"

int main(int argc, char *args[])
{
	Application E219Prototype = Application();
    
	E219Prototype.init();
    
	while (E219Prototype.run())
		continue;
    
	return 0;
}