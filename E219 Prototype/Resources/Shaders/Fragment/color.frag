// Fragment Shader � file "color.frag"
// outputs a passed in color main use is for debugging, normally used in conjunction with colorMVP.vert

#version 330

precision highp float; // needed only for version 1.30

in  vec4 ex_Color;
out vec4 out_Color;

void main(void) {
	out_Color = ex_Color;
}
