// deferredfirstpass.frag
#version 330

// Some drivers require the following
precision highp float;

uniform sampler2D textureUnit0;

in vec2 ex_TexCoord;
in vec3 ex_Normal;
in vec3 ex_Position;

out vec4 out_Position;
out vec4 out_Normal;
out vec4 out_Color;
out vec4 out_Ambient;

// can do the deffered lighting calculations in view or world, currently doing them
// in world, better for post proccessing algorithms in a lot of cases, Cry Engine and UE3 use it 
// this way
void main(void) {

	// the w component 1.0 really doesn't matter, its a nonsense value used so that we 
	// can render the buffer results to the screen! Doesn't seem to like rendering RGB textures.
	out_Position = vec4(ex_Position, 1.0); // output view space position
	out_Normal = vec4(ex_Normal, 1.0); // output view space normal  
	out_Color = texture(textureUnit0, ex_TexCoord); // the diffuse colour of the fragment
	out_Ambient = vec4(0.3, 0.3, 0.3, 1.0) * out_Color;
}