// textured.frag
#version 330

// Some drivers require the following
precision highp float;

// creating and defining a light
struct Light{
	vec4 diffuse;
	vec4 specular;
	float radius; 
};

uniform Light light;
uniform vec3 lightPosition;

// g-buffer textures
uniform sampler2D diffuseTexture;
uniform sampler2D positionTexture;
uniform sampler2D normalTexture;

// screen resolution 800/400 etc.
uniform vec2 screenResolution;

uniform vec3 cameraPos; // doing it in world space co-ordinates, camera is not at 0,0,0 like view space, can't just negate position 

out vec4 out_Color;

// hard coded material information for testing
vec4 kd = vec4(0.4, 0.4, 0.4, 1.0); // diffuse
vec4 ks = vec4(0.3, 0.3, 0.3, 1.0); // specular
float shininess = 2.0f; 

// can do the deffered lighting calculations in view or world, currently doing them
// in world, better for post proccessing algorithms in a lot of cases, Cry Engine and UE3 use it 
// this way
void main(void) {

	// gl_FragCoord.xy gets the window space position of the fragment
	// which is for instance x = 0 - 800 or whatever depending on our 
	// current screen size. We divide it by the screen resolution to 
	// get normalized co-ordinates in a 0-1 range that we can then use
	// to query the textures to find the relative information for that pixel
	// on the screen! need to pass in the screenResolution as a uniform however
	// since OpenGL has no built in variable for it
	vec2 tex_Coord = gl_FragCoord.xy / screenResolution;  

	// pulling data for fragments out of our G-Buffer textures
	vec3 normal = normalize(texture(normalTexture, tex_Coord).xyz);
	vec3 position = texture(positionTexture, tex_Coord).xyz;
	vec4 diffuse = normalize(texture(diffuseTexture, tex_Coord));

	// Phong Shading Shtoof

	vec3 posToLight = normalize(lightPosition - position); // frag to light 
	
	// DIFFUSE, diffuse is the angle of light on the material surface without contribution from the viewer
	// phong diffuse theory goes a little something like "irregardless of the angle of viewing from the user
	// the light contribution will always remain the same as the viewer will either see less of the surface with a higher
	// percentage of reflection(looking edge on) or more of the surface with a lesser percentage of reflection(head on), either way
	// light remains overall the same and isn't effected by the viewer" my in-elegant way of explaining it.   
	
	float diffuseLight = max(dot(normal, posToLight), 0); // diffuse light contribution, max cause we no wanty -ve light
	vec4 diffuseColor = light.diffuse * kd * diffuseLight; 

	// SPECULAR
	// angle of incidence is negative pos to light since pos to light is the opposite direction..
	vec3 lightReflection = reflect(-posToLight, normal);

	// specular is based off the viewers position and the angle of reflection to the viewer I.E directly into the viewers eye 
	// should = really shiny completely far away from the reflection would be next to no specular highlight. We raise it to the power
	// of the shininess of the material, shiny material should give more powerful specular highlights etc. 
	vec4 specularColor = light.specular * ks * pow(max(dot(normalize(cameraPos - position), normalize(lightReflection)), 0), shininess); 
	
	// fake attenuation based on light radius for now(might keep it if it looks better than actual attenuation in the end, graphic
	// programming is all fakery anyway)  
//	float attenuation = length(lightPosition - position) / light.radius; 
	 

	// yeah, yeah specular is working alongside diffuse...but hey don't have a specular texture map yet
	// and the regular texture is apparently the diffuse texture
	out_Color = vec4((specularColor.rgb + diffuseColor.rgb /*+ vec3(0.1, 0.1, 0.1)*/) * diffuse.rgb, diffuse.a);
}