// MVP.vert
// used when all you want to pass through is the models position
// Useful for depth buffer writing, Stencil only passes and used
// as the vertex shader of the deferredsecondpass program alongside the deferred fragment shader
// Also used in the stencil pass section of the deferred renderer
#version 330

uniform mat4 MVP;

in vec3 in_Position;


// simple pass through data shader into the g-buffer
void main(void) {
	// still need to calculate and pass the position in clip space to caclulate the depth for the g-buffer
    gl_Position = MVP * vec4(in_Position, 1.0);	
	
}