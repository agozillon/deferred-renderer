// deferredfirstpass.vert
// passes all the information thats required for deferred rendering into the G-Buffer in world space
// co-ordinates
#version 330

uniform mat4 MVP;
uniform mat4 model;
uniform mat3 normalmatrix;

in vec3 in_Position;
in vec3 in_Normal;
in vec2 in_TexCoord;

out vec2 ex_TexCoord;
out vec3 ex_Normal;
out vec3 ex_Position;

// simple pass through data shader into the g-buffer
void main(void) {

	ex_Position = (model * vec4(in_Position, 1.0)).xyz; // the co-ordinate we output to the G-buffer, world space position
	ex_Normal = normalize(normalmatrix * in_Normal); // calculating the world space normal
	 
	ex_TexCoord = in_TexCoord; // just pass the tex co-ords through

	// still need to calculate and pass the position in clip space to caclulate the depth for the g-buffer
    gl_Position = MVP * vec4(in_Position, 1.0);
	
}