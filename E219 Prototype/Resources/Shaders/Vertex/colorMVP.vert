// Vertex Shader � file "colorMVP.vert"
// used for debugging and that's about it, used in conjunction with color.frag

#version 330

uniform mat4 projection;
uniform mat4 modelview;

in vec3 in_Position;
in  vec3 in_Color;

out vec4 ex_Color;

void main(void)
{
	ex_Color = vec4(in_Color, 1.0);
	gl_Position =  projection * modelview * vec4(in_Position, 1.0);
}