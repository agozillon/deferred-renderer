#pragma once

class Audio
{
public:
	/* Virtual Destructor */
	virtual ~Audio() {}

public:
	/* General Public Methods */
	virtual void update() = 0;

public:
	/* Audio Management */
	virtual void loadSound(const char* sound, const char* filePath, bool streamed) = 0;
	virtual void playSound(const char* sound, bool loop, unsigned int volume) = 0;
	virtual void pauseSound(const char* sound) = 0;
	virtual void resumeSound(const char* sound) = 0;

	virtual void stopSound(const char* sound) = 0;
	virtual void stopAllSounds() = 0;

	virtual void muteSound(const char* sound) = 0;
	virtual void muteAllSounds() = 0;

	virtual void setSoundVolume(const char* sound, unsigned int volume) = 0;
	virtual void setMasterVolume(unsigned int volume) = 0;
};

