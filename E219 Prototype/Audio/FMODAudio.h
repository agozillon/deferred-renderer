#pragma once

#include <iostream>
#include <unordered_map>

#ifdef _WIN32
#include <fmod.hpp>
#include <fmod_errors.h>
#elif __APPLE__
#include "fmod.hpp"
#include "fmod_errors.h"
#endif

#include "Audio.h"

class FMODAudio : public Audio
{
public:
	/* Class Constructor & Destructor */
	FMODAudio();
	virtual ~FMODAudio();

public:
	/* General Public Methods */
	virtual void update();

public:
	/* Audio Management */
	virtual void loadSound(const char* sound, const char* filePath, bool streamed);
	virtual void playSound(const char* sound, bool loop, unsigned int volume);
	virtual void pauseSound(const char* sound);
	virtual void resumeSound(const char* sound);

	virtual void stopSound(const char* sound);
	virtual void stopAllSounds();

	virtual void muteSound(const char* sound);
	virtual void muteAllSounds();

	virtual void setSoundVolume(const char* sound, unsigned int volume);
	virtual void setMasterVolume(unsigned int volume);

private:
	/* FMOD Member Methods */
	void createSystem();
	void FMODErrorCheck(FMOD_RESULT result);
	void FMODCheckVersion();
	void checkDrivers();

	FMOD::Sound* createSound(const char* filePath);
	FMOD::Sound* streamSound(const char* filePath);

private:
	/* FMOD Member Variables */
	FMOD::System* m_system;
	FMOD_RESULT m_result;
	FMOD::Channel* m_channel;
	FMOD_SPEAKERMODE m_speakerMode;
	FMOD_CAPS m_caps;

	unsigned int m_version;
	int m_numDrivers;
	char m_name[256];

	std::unordered_map<const char*, FMOD::Sound*> m_soundMap;
};

