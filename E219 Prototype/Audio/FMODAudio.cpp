#include "FMODAudio.h"

#pragma region Class Constructor & Destructor
/*
* Constructs FMODAudio class.
*/
FMODAudio::FMODAudio()
{
	createSystem();
	FMODCheckVersion();
	checkDrivers();

	std::cout << "FMODAudio constructed." << std::endl;
}

/*
* Destructs FMODAudio class.
*/
FMODAudio::~FMODAudio()
{
	/* Release all sounds */
	for (auto iterator = m_soundMap.begin(); iterator != m_soundMap.end(); ++iterator)
	{
		iterator->second->release();
	}

	/* Release the system */
	m_system->release();

	std::cout << "FMODAudio destructed." << std::endl;
}

#pragma endregion

#pragma region General Public Methods
/*
* Updates the FMOD system.
*/
void FMODAudio::update()
{
	m_system->update();
}

#pragma endregion

#pragma region Audio Management
/*
* Adds new sound files into the sound map and creates an FMOD sound to be used later.
*
* @param const char* sound - name of the sound.
* @param const char* filePath - path to the actual sound file.
* @param bool streamed - if the sound should be streamed or not.
*/
void FMODAudio::loadSound(const char* sound, const char* filePath, bool streamed)
{
	if (streamed == false)
	{
		m_soundMap[sound] = createSound(filePath);
	}
	else
	{
		m_soundMap[sound] = streamSound(filePath); 
	}

	std::cout << "FMODAudio: Sound loaded: " << sound << " (" << filePath << ")." << std::endl;
}

/*
* Plays a sound.
*
* @param const char* sound - the sound to be played.
* @param bool loop - true if sound should be looped, false if not.
* @param unsigned int volume - the volume to play the sound at.
*/
void FMODAudio::playSound(const char* sound, bool loop, unsigned int volume)
{
	if (loop == true)
	{
		m_result = m_system->playSound(FMOD_CHANNEL_FREE, m_soundMap[sound], false, &m_channel);
		FMODErrorCheck(m_result);
		m_channel->setLoopCount(-1);
		m_channel->setMode(FMOD_LOOP_NORMAL);
		m_channel->setPosition(0, FMOD_TIMEUNIT_MS);
	}
	else
	{
		m_result = m_system->playSound(FMOD_CHANNEL_FREE, m_soundMap[sound], false, 0);
		FMODErrorCheck(m_result);
	}
}

/*
* Pauses a sound.
*
* @param const char* sound - the sound to be paused.
*/
void FMODAudio::pauseSound(const char* sound)
{
	m_result = m_channel->setPaused(true);
	std::cout << "FMODAudio: Sound paused: " << sound << std::endl;
}

/*
* Resumes a sound.
*
* @param const char* sound - the sound to be resumed.
*/
void FMODAudio::resumeSound(const char* sound)
{
	//m_result = m_channel->setPaused(false);
	std::cout << "FMODAudio: Sound resumed: " << sound << std::endl;
}

/*
* Stops a sound.
*
* @param const char* sound - the sound to be stopped.
*/
void FMODAudio::stopSound(const char* sound)
{
}

/*
* Stops all sounds.
*/
void FMODAudio::stopAllSounds()
{
}

/*
* Mutes a sound.
*
* @param const char* sound - the sound to be muted.
*/
void FMODAudio::muteSound(const char* sound)
{
}

/*
* Mutes all sounds.
*/
void FMODAudio::muteAllSounds()
{
}

/*
* Sets the volume of a sound.
*
* @param const char* sound - the sound to have its volume set.
* @param unsigned int volume - the volume to set the sound to.
*/
void FMODAudio::setSoundVolume(const char* sound, unsigned int volume)
{
}

/*
* Sets master volume.
*
* @param unsigned int volume - the volume to set the master volume to.
*/
void FMODAudio::setMasterVolume(unsigned int volume)
{
}

#pragma endregion

#pragma region FMOD Member Methods
/*
* Create system object.
*/
void FMODAudio::createSystem()
{
	//m_result = FMOD::System_Create(&m_system);
	FMODErrorCheck(m_result);
}

/*
* Used to check for errors in any FMOD functions.
* @param FMOD_RESULT result - used to get resulting error from function to output them to console.
*/
void FMODAudio::FMODErrorCheck(FMOD_RESULT result)
{
	if (result != FMOD_OK)
	{
		std::cout << "FMOD Error: (" << result << ") " << FMOD_ErrorString(result) << std::endl;
	}
}

/*
* Checks version of FMOD on the system. 
* Error message will display if version is out of date and will display required version.
*/
void FMODAudio::FMODCheckVersion()
{
	//m_result = m_system->getVersion(&m_version);
	FMODErrorCheck(m_result);

	if (m_version < FMOD_VERSION)
	{
		std::cout << "Error! You are using an old version of FMOD " << m_version << ". This program requires " << FMOD_VERSION << std::endl;
		return;
	}
}

/*
* Checks drivers and sound cards detected on system, sets correct modes from results.
* Code from FMOD documentation for getting started for Windows.
*/
void FMODAudio::checkDrivers()
{
	m_result = m_system->getNumDrivers(&m_numDrivers);
	FMODErrorCheck(m_result);
	if (m_numDrivers == 0)
	{
		m_result = m_system->setOutput(FMOD_OUTPUTTYPE_NOSOUND);
	}
	else
	{
		m_result = m_system->getDriverCaps(0, &m_caps, 0, &m_speakerMode);
		FMODErrorCheck(m_result);

		//Set the user selected speaker mode.
		m_result = m_system->setSpeakerMode(m_speakerMode);
		FMODErrorCheck(m_result);
		if (m_caps & FMOD_CAPS_HARDWARE_EMULATED)
		{
			/*
			The user has the 'Acceleration' slider set to off! This is really bad
			for latency! You might want to warn the user about this.
			*/
			m_result = m_system->setDSPBufferSize(1024, 10);
			FMODErrorCheck(m_result);
		}
		m_result = m_system->getDriverInfo(0, m_name, 256, 0);
		FMODErrorCheck(m_result);
		if (strstr(m_name, "SigmaTel"))
		{
			/*
			Sigmatel sound devices crackle for some reason if the format is PCM 16bit.
			PCM floating point output seems to solve it.
			*/
			m_result = m_system->setSoftwareFormat(48000, FMOD_SOUND_FORMAT_PCMFLOAT, 0, 0,
				FMOD_DSP_RESAMPLER_LINEAR);
			FMODErrorCheck(m_result);
		}

	}
	m_result = m_system->init(100, FMOD_INIT_NORMAL, 0);
	if (m_result == FMOD_ERR_OUTPUT_CREATEBUFFER)
	{
		/*
		Ok, the speaker mode selected isn't supported by this soundcard. Switch it
		back to stereo...
		*/
		m_result = m_system->setSpeakerMode(FMOD_SPEAKERMODE_STEREO);
		FMODErrorCheck(m_result);
		/*
		... and re-init.
		*/
		m_result = m_system->init(100, FMOD_INIT_NORMAL, 0);
	}
	FMODErrorCheck(m_result);
}

/*
* Creates a sound, decompressing it into memory. Used for small sound files. Can use a lot of memory.
*
* @param const char* filePath - the file of the sound file to be created.
* @return FMOD::Sound* - pointer to the FMOD sound type created for the sound.
*/
FMOD::Sound* FMODAudio::createSound(const char* filePath)
{
	FMOD::Sound* tempSound;

	m_result = m_system->createSound(filePath, FMOD_DEFAULT, 0, &tempSound);
	FMODErrorCheck(m_result);
	return tempSound;
}

/*
* Streams a sound to load and play large sound file. Uses less memory but more CPU power as it decodes at run time.
*
* @param const char* filePath - the file of the sound file to be streamed.
* @return FMOD::Sound* - pointer to the FMOD sound type created for the sound.
*/
FMOD::Sound* FMODAudio::streamSound(const char* filePath)
{
	FMOD::Sound* tempSound;

	m_result = m_system->createStream(filePath, FMOD_DEFAULT, 0, &tempSound);
	FMODErrorCheck(m_result);
	return tempSound;
}

#pragma endregion