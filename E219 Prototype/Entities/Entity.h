//
//  Entity.h
//  E219 Prototype
//
//  Created by Marty on 08/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#pragma once

#include <iostream>
#include <unordered_map>

class Entity
{
    /* Class Constructors & Destructors */
public:
    Entity(unsigned int entityID, unsigned int maximumComponents);
    ~Entity();
    
    /* Entity Management */
public:
    void setEntityName(const char* entityName);
    const char* getEntityName();
    unsigned int getEntityID();
    bool isAssigned();
    void assign();
    void unassign();
    
private:
    const char* m_entityName;
    unsigned int m_entityID;
    bool m_assigned;
    
    /* Component Management */
public:
	bool addComponent(const char* componentType, unsigned int componentIndex);
	unsigned int getComponent(const char* componentType);
    
private:
	std::unordered_map<const char*, unsigned int> m_componentMap;
};

