//
//  EntityManager.h
//  E219 Prototype
//
//  Created by Marty on 07/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#pragma once

#include <vector>

#include "Entity.h"
#include "Components/ComponentManager.h"

class EntityManager
{
    /* Class Constructor & Destructor */
public:
	EntityManager(unsigned int maximumEntities, unsigned int maximumComponents);
    ~EntityManager();
    
    /* Entity Management */
public:
    void createEntity(const char* entityName);
	void addComponentToEntityID(unsigned int entityID, const char* componentType);
	void addComponentToEntityName(const char* entityName, const char* componentType);
	Entity getEntityReference(unsigned int entityID);
	bool entityHasComponent(unsigned int entityID, const char* componentType);
	int getEntityCount(){ return m_entityVector.size(); }
	int getAllocatedEntityCount(){ return m_activeEntities; }
	WorldPlacement2DComponent* getWorldPlacement2DComponent(unsigned int entityID, const char* componentType);
	WorldPlacement3DComponent* getWorldPlacement3DComponent(unsigned int entityID, const char* componentType);
	LightComponent* getLightComponent(unsigned int entityID);
	ModelComponent* getModelComponent(unsigned int entityID);

	WorldPlacement2DComponent* getWorldPlacement2DComponent(const char* entityName, const char* componentType);
	WorldPlacement3DComponent* getWorldPlacement3DComponent(const char* entityName, const char* componentType);
	ModelComponent* getModelComponent(const char* entityName);
	LightComponent* getLightComponent(const char* entityName);

private:
	void addComponentToEntity(unsigned int entity, const char* componentType);

    std::vector<Entity> m_entityVector;
    
    ComponentManager m_componentManager;
	int m_activeEntities;

private:
    bool entityVectorFull();
    bool checkUniqueName(const char* entityName);
};