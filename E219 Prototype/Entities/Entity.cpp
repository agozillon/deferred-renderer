//
//  Entity.cpp
//  E219 Prototype
//
//  Created by Marty on 08/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#include "Entity.h"

#pragma region Class Constructors & Destructors
/*
 * Constructs the Entity class.
 *
 * @param unsigned int entityID - an integer ID to identify the Entity.
 * @param unsigned int maximumComponents - the maximum number of Components an Entity can hold.
 */
Entity::Entity(unsigned int entityID, unsigned int maximumComponents)
{
    m_assigned = false;
    m_entityName = "";
    
    m_entityID = entityID;

	m_componentMap.reserve(maximumComponents);

	std::cout << std::endl << "Entity Constructed (ID: " << m_entityID << ")." << std::endl;
}

/*
 * Destructs the Component class.
 */
Entity::~Entity()
{
}

#pragma endregion

#pragma region Entity Management
/*
 * Set the name of the Entity.
 *
 * @param const char* entityName - the name to use for the Entity.
 */
void Entity::setEntityName(const char* entityName)
{
    m_entityName = entityName;
	std::cout << std::endl << "Entity (ID: " << m_entityID << ") name set: " << m_entityName << std::endl;
}

/*
 * Get the name of the Entity.
 *
 * @return const char* - the name stored by the Entity.
 */
const char* Entity::getEntityName()
{
    return m_entityName;
}

/*
 * Get the Entity ID.
 *
 * @return unsigned int - the ID stored by the Entity.
 */
unsigned int Entity::getEntityID()
{
    return m_entityID;
}

/*
 * Returns true or false signifying if the Entity has been assigned from the Entity pool.
 *
 * @return bool - the boolean used to determine Entity assignment.
 */
bool Entity::isAssigned()
{
    return m_assigned;
}

/*
 * Sets the assignment of the Entity to true (Entity can now be used by the game).
 */
void Entity::assign()
{
    if (m_assigned == false)
    {
        m_assigned = true;
        
        if (m_entityName == nullptr)
        {
			std::cout << std::endl << "Entity (ID: " << m_entityID << ") assigned." << std::endl;
        }
        else
        {
			std::cout << std::endl << "Entity (ID: " << m_entityID << ", Name: " << m_entityName << ") assigned." << std::endl;
        }
    }
    else
    {
        if (m_entityName == nullptr)
        {
			std::cout << std::endl << "Entity (ID: " << m_entityID << ") cannot be assigned. It is already assigned." << std::endl;
        }
        else
        {
			std::cout << std::endl << "Entity (ID: " << m_entityID << ", Name: " << m_entityName << ") cannot be assigned. It is already assigned." << std::endl;
        }
    }
}

/*
 * Sets the assignment of the Entity to false (Entity now cannot be used by the game).
 */
void Entity::unassign()
{
    m_assigned = false;
    
    if (m_entityName == nullptr)
    {
		std::cout << std::endl << "Entity (ID: " << m_entityID << ") unassigned." << std::endl;
    }
    else
    {
		std::cout << std::endl << "Entity (ID: " << m_entityID << ", Name: " << m_entityName << ") unassigned." << std::endl;
    }
}

#pragma endregion


#pragma region Component Management
/*
 * Add a Component to the Entity.
 *
 * @param const char* componentType - the type of the Component being added.
 * @param unsigned int componentIndex - the index of the Component from its container.
 *
 * @return bool - true if the Component was added, else false if not.
 */
bool Entity::addComponent(const char* componentType, unsigned int componentIndex)
{
	bool addedSuccessfully = false;

	bool slotAvailable = false;

	if (m_componentMap.find(componentType) == m_componentMap.end())
	{
		slotAvailable = true;
	}
	
	if (slotAvailable == true)
	{
		m_componentMap[componentType] = componentIndex;
		addedSuccessfully = true;

		if (m_entityName != nullptr)
		{
			std::cout << std::endl << "Added Component (Type: " << componentType << ", Index: " << componentIndex << ") to Entity (ID: " << m_entityID << ", Name: " << m_entityName << ")." << std::endl;
		}
		else 
		{
			std::cout << std::endl << "Added Component (Type: " << componentType << ", Index: " << componentIndex << ") to Entity (ID: " << m_entityID << ")." << std::endl;
		}
	}
	else
	{
		if (m_entityName != nullptr)
		{
			std::cout << std::endl << "Could not add Component (Type: " << componentType << ") to Entity (ID: " << m_entityID << ", Name: " << m_entityName << "). Entity already has a Component of this type." << std::endl;
		}
		else
		{
			std::cout << std::endl << "Could not add Component (Type: " << componentType << ") to Entity (ID: " << m_entityID << "). Entity already has a Component of this type." << std::endl;
		}
	}

	return addedSuccessfully;
}

/*
 * Returns a pointer to a Component held by the Entity.
 *
 * @param ComponentManager::ComponentType componentType - the type of Component to return.
 * @return @std::shared_ptr<Component> - a pointer to the Component requested.
 */
unsigned int Entity::getComponent(const char* componentType)
{
    unsigned int component = 0;
    
	if (m_componentMap.find(componentType) != m_componentMap.end())
	{
		component = m_componentMap[componentType];
	}
	else
	{
		if (m_entityName != nullptr)
		{
			std::cout << std::endl << "Could not get Component (Type: " << componentType << ") from Entity (ID: " << m_entityID << ", Name: " << m_entityName << "). Entity does not have this Component type." << std::endl;
		}
		else
		{
			std::cout << std::endl << "Could not get Component (Type: " << componentType << ") from Entity (ID: " << m_entityID << "). Entity does not have this Component type." << std::endl;
		}
	}
    
    return component;
}

#pragma endregion