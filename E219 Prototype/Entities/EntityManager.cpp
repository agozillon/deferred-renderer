//
//  EntityManager.cpp
//  E219 Prototype
//
//  Created by Marty on 07/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#include "EntityManager.h"

#pragma region Class Constructor & Destructor
/*
 * Constructs the EntityManager class.
 *
 * @param unsigned int maximumEntities - the maximum number of Entites which should be created.
 * @param unsigned int maximumEntities - the maximum number of Components which should be created.
 */
EntityManager::EntityManager(unsigned int maximumEntities, unsigned int maximumComponents)
{
	m_entityVector.reserve(maximumEntities);
	m_activeEntities = 1;

	for (unsigned int index = 0; index < maximumEntities; ++index)
    {
		m_entityVector.emplace_back(index, maximumComponents);
    }
    
	m_componentManager = ComponentManager(maximumComponents);
}

/*
 * Destructs the EntityManager class.
 */
EntityManager::~EntityManager()
{
    
}

#pragma endregion

#pragma region Entity Management
/*
 * Sets up a new Entity object.
 */
void EntityManager::createEntity(const char* entityName)
{
    if (entityVectorFull() == false)
    {
        if (checkUniqueName(entityName))
        {
            for (std::vector<Entity>::iterator iterator = m_entityVector.begin() + 1; iterator != m_entityVector.end(); ++iterator)
            {
                if (iterator->isAssigned() == false)
                {
                    iterator->assign();
                    iterator->setEntityName(entityName);
					m_activeEntities++;

                    break;
                }
            }
        }
        else
        {
            std::cout << std::endl << "Could not create Entity: " << entityName << ", another Entity exists with this name already." << std::endl;
        }
    }
    else
    {
        std::cout << std::endl << "Could not create Entity: " << entityName << ", Entity Vector is full." << std::endl;
    }
}

/*
 * Returns a copy of the component at the current Entity ID
 *
 * @param unsigned int entityID - unique integer ID to identify the Entity
 *
 */
Entity EntityManager::getEntityReference(unsigned int entityID)
{
	return m_entityVector[entityID];
}

/* 
 * checks if the entity owns the component and returns true if it does, else returns false
 * @param unsigned int entityID - unique integer ID to identify the Entity
 * @param const char* componentType componentType - the type of component to check for on the Entity.
 */
bool EntityManager::entityHasComponent(unsigned int entityID, const char* componentType)
{
	if (m_entityVector[entityID].getComponent(componentType) > 0)
		return true;
	else
		return false;
}

/*
* returns the specified component type for the current entity ID 
* @param unsigned int entityID - unique integer ID to identify the Entity
* @param const char* componentType componentType - the type of component to check for on the Entity.
* @return returns a pointer for the specified component of the entityID passed in if it exists
*/
WorldPlacement3DComponent* EntityManager::getWorldPlacement3DComponent(unsigned int entityID, const char* componentType)
{
	return m_componentManager.getWorldPlacement3DComponent(m_entityVector[entityID].getComponent(componentType));
}

/*
* returns the specified component type for the current entity ID
* @param unsigned int entityID - unique integer ID to identify the Entity
* @param const char* componentType componentType - the type of component to check for on the Entity.
* @return returns a pointer for the specified component of the entityID passed in if it exists
*/
WorldPlacement2DComponent* EntityManager::getWorldPlacement2DComponent(unsigned int entityID, const char* componentType)
{
	return m_componentManager.getWorldPlacement2DComponent(m_entityVector[entityID].getComponent(componentType));
}

/*
* returns the specified component type for the current entity ID
* @param unsigned int entityID - unique integer ID to identify the Entity
* @return returns a pointer for the specified component of the entityID passed in if it exists
*/
LightComponent* EntityManager::getLightComponent(unsigned int entityID)
{
	return m_componentManager.getLightComponent(m_entityVector[entityID].getComponent("LIGHT"));
}

/*
* returns the specified component type for the current entity ID
* @param unsigned int entityID - unique integer ID to identify the Entity
* @return returns a pointer for the specified component of the entityID passed in if it exists
*/
ModelComponent* EntityManager::getModelComponent(unsigned int entityID)
{
	return m_componentManager.getModelComponent(m_entityVector[entityID].getComponent("MODEL"));
}

/*
* returns the specified component type for the current entity name
* @param const char* entityName - unique name to identify the Entity
* @param const char* componentType - name of the component
* @return returns a pointer for the specified component of the entityName passed in if it exists
*/
WorldPlacement3DComponent* EntityManager::getWorldPlacement3DComponent(const char* entityName, const char* componentType)
{
	for (unsigned int index = 1; index < m_entityVector.size(); ++index)
	{
		if ((std::string)m_entityVector[index].getEntityName() == (std::string)entityName)
		{
			return m_componentManager.getWorldPlacement3DComponent(m_entityVector[index].getComponent(componentType));
		}
	}
}

/*
* returns the specified component type for the current entity name
* @param const char* entityName - unique name to identify the Entity
* @param const char* componentType - name of the component
* @return returns a pointer for the specified component of the entityName passed in if it exists
*/
WorldPlacement2DComponent* EntityManager::getWorldPlacement2DComponent(const char* entityName, const char* componentType)
{
	for (unsigned int index = 1; index < m_entityVector.size(); ++index)
	{
		if (m_entityVector[index].getEntityName() == entityName)
		{
			return m_componentManager.getWorldPlacement2DComponent(m_entityVector[index].getComponent(componentType));
		}
	}
}

/*
* returns the specified component type for the current entity name
* @param const char* entityName - unique name to identify the Entity
* @return returns a pointer for the specified component of the entityName passed in if it exists
*/
ModelComponent* EntityManager::getModelComponent(const char* entityName)
{
	for (unsigned int index = 1; index < m_entityVector.size(); ++index)
	{
		if (m_entityVector[index].getEntityName() == entityName)
		{
			return m_componentManager.getModelComponent(m_entityVector[index].getComponent("MODEL"));	
		}
	}
}

/*
* returns the specified component type for the current entity name
* @param const char* entityName - unique name to identify the Entity
* @return returns a pointer for the specified component of the entityName passed in if it exists
*/
LightComponent* EntityManager::getLightComponent(const char* entityName)
{
	for (unsigned int index = 1; index < m_entityVector.size(); ++index)
	{
		if (m_entityVector[index].getEntityName() == entityName)
		{
			return m_componentManager.getLightComponent(m_entityVector[index].getComponent("LIGHT"));
		}
	}
}


/*
 * Adds a Component to an Entity via its ID.
 *
 * @param unsigned int entityID - unique integer ID to identy the Entity.
 * @param const char* componentType componentType - the type of component to add to the Entity.
 */
void EntityManager::addComponentToEntityID(unsigned int entityID, const char* componentType)
{
	for (unsigned int index = 1; index < m_entityVector.size(); ++index)
	{
		if (m_entityVector[index].getEntityID() == entityID)
		{
			addComponentToEntity(index, componentType);
			break;
		}
	}
}

/*
* Adds a Component to an Entity via its Name.
*
* @param const char* entityName - unique name string to identy the Entity.
* @param const char* componentType - the type of component to add to the Entity.
*/
void EntityManager::addComponentToEntityName(const char* entityName, const char* componentType)
{
	for (unsigned int index = 1; index < m_entityVector.size(); ++index)
	{
		if (m_entityVector[index].getEntityName() == entityName)
		{
			addComponentToEntity(index, componentType);
			break;
		}
	}
}

/*
* Adds a Component to an Entity.
*
* @param unsigned int entity - index of the Entity to add a Component to.
* @param Component::ComponentType componentType - the type of Component to add to the Entity.
*/
void EntityManager::addComponentToEntity(unsigned int entity, const char* componentType)
{
	unsigned int componentIndex;

	componentIndex = m_componentManager.orderComponent(componentType);
	
	if (m_entityVector[entity].addComponent(componentType, componentIndex) == true)
	{
		m_componentManager.setComponentOwner(componentType, componentIndex, m_entityVector[entity].getEntityName(), entity);
		m_componentManager.assignComponent(componentType, componentIndex);
	}
}

/*
 * Determine if all Entities in Entity Vector have been assigned.
 *
 * @return bool - true or false if all entities are assigned.
 */
bool EntityManager::entityVectorFull()
{
    bool vectorIsFull = false;
    int numberOfEntities = 0;
    
	for (unsigned int index = 1; index < m_entityVector.size(); ++index)
    {
        if (m_entityVector[index].isAssigned() == true)
        {
            ++numberOfEntities;
        }
    }
    
	if (numberOfEntities == m_entityVector.size())
    {
        vectorIsFull = true;
    }
    
    return vectorIsFull;
}

/*
 * Determine if Entity name is unique.
 *
 * @param const char* entityName - name of the Entity to be tested.
 * @return bool - true if name of Entity is unique or false if not.
 */
bool EntityManager::checkUniqueName(const char* entityName)
{
    bool uniqueName = true;
    
	for (unsigned int index = 1; index < m_entityVector.size(); ++index)
    {
        if (m_entityVector[index].getEntityName() == entityName)
        {
            uniqueName = false;
        }
    }
    
    return uniqueName;
}

#pragma endregion