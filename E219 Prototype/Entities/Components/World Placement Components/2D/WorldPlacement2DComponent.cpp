//
//  Placement2DComponent.cpp
//  E219 Prototype
//
//  Created by Marty on 08/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#include "WorldPlacement2DComponent.h"

#pragma region Class Constructors & Destructors

/*
 * Constructs the WorldPlacement2DComponent class.
 *
 * @param unsigned int componentID - integer ID used to identify the Component.
 */
WorldPlacement2DComponent::WorldPlacement2DComponent(unsigned int componentID, const char* placementType)
{
	m_world2DPlacement[0] = 0; 	m_world2DPlacement[1] = 0;
	m_componentID = componentID;
	m_type = placementType;
}


#pragma endregion

