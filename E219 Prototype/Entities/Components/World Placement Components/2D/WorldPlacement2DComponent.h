//
//  PositionComponent.h
//  E219 Prototype
//
//  Created by Marty on 08/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#pragma once

#include "../../Component.h"

class WorldPlacement2DComponent : public Component
{
    /* Class Constructors & Destructors */
public:
    WorldPlacement2DComponent() { }
    WorldPlacement2DComponent(unsigned int componentID, const char* placementType);
	virtual ~WorldPlacement2DComponent(){ }
	WorldPlacement2DComponent(unsigned int componentID){ m_componentID = componentID; }

public:
    /* Position Component Management */
	float* getWorld2DPlacement(){ return m_world2DPlacement; }
	float getWorld2DPlacementX(){ return m_world2DPlacement[0]; }
	float getWorld2DPlacementY(){ return m_world2DPlacement[1]; }

	void setWorld2DPlacementX(float newWorld2DPlacementX){ m_world2DPlacement[0] = newWorld2DPlacementX; }
	void setWorld2DPlacementY(float newWorld2DPlacementY){ m_world2DPlacement[1] = newWorld2DPlacementY; }
	void setWorld2DPlacement(float newWorld2DPlacementX, float newWorld2DPlacementY){ m_world2DPlacement[0] = newWorld2DPlacementX; m_world2DPlacement[1] = newWorld2DPlacementY; }
	void setType(const char* type){ m_type = type; }

private:
    float m_world2DPlacement[2];
};