#pragma once

#include "../../Component.h"

class WorldPlacement3DComponent : public Component
{
	/* Class Constructors & Destructors */
public:
	WorldPlacement3DComponent() { }
	WorldPlacement3DComponent(unsigned int componentID, const char* placementType);
	virtual ~WorldPlacement3DComponent(){ }
	WorldPlacement3DComponent(unsigned int componentID){ m_componentID = componentID; }

public:
	/* World3DPlacement Component Management */
	// Gets
	float* getWorld3DPlacement(){ return m_world3DPlacement; }
	float getWorld3DPlacementX(){ return m_world3DPlacement[0]; }
	float getWorld3DPlacementY(){ return m_world3DPlacement[1]; }
	float getWorld3DPlacementZ(){ return m_world3DPlacement[2]; }

	// Sets
	void setWorld3DPlacementX(float newWorld3DPlacement){ m_world3DPlacement[0] = newWorld3DPlacement; }
	void setWorld3DPlacementY(float newWorld3DPlacement){ m_world3DPlacement[1] = newWorld3DPlacement; }
	void setWorld3DPlacementZ(float newWorld3DPlacement){ m_world3DPlacement[2] = newWorld3DPlacement; }

	void setWorld3DPlacement(float newWorld3DPlacementX, float newWorld3DPlacementY, float newWorld3DPlacementZ){ m_world3DPlacement[0] = newWorld3DPlacementX; m_world3DPlacement[1] = newWorld3DPlacementY; m_world3DPlacement[2] = newWorld3DPlacementZ; }
	void setType(const char* type){ m_type = type; } // changes the type, largelly specific to this class as it can be used for multiple things (rotation/scalar/pos)

private:
	float m_world3DPlacement[3];
};