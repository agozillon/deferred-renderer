#include "WorldPlacement3DComponent.h"

#pragma region Class Constructors & Destructors

/*
* Constructs the WorldPlacement2DComponent class.
*
* @param unsigned int componentID - integer ID used to identify the Component.
*/
WorldPlacement3DComponent::WorldPlacement3DComponent(unsigned int componentID, const char* placementType)
{
	m_world3DPlacement[0] = 0; m_world3DPlacement[1] = 0; m_world3DPlacement[2] = 0;
	m_componentID = componentID;
	m_type = placementType;
}


#pragma endregion