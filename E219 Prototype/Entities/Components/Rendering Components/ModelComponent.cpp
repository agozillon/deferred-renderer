#include "ModelComponent.h"

ModelComponent::ModelComponent(unsigned int componentID, GLuint meshID, GLuint indexCount, std::shared_ptr<Texture2D> textureID)
: m_meshID(meshID), m_indexCount(indexCount), m_texture(textureID)
{
	m_componentID = componentID;
	m_type = "MODEL";
}