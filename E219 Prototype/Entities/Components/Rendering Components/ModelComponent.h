#pragma once
#include <gl\glew.h>
#include <memory>
#include "../../../Renderer/Texture/Texture2D.h"
#include "../Component.h"

// Basic Model component that holds a mesh and texture, all that's currently required for
// a model at the moment
class ModelComponent : public Component
{
public:
	ModelComponent(unsigned int componentID, GLuint meshID, GLuint indexCount, std::shared_ptr<Texture2D> textureID);
	ModelComponent(){ }
	ModelComponent(unsigned int componentID){ m_componentID = componentID; m_type = "MODEL"; }
	virtual ~ModelComponent(){ }

	// gets for the stored mesh id and texture
	GLuint getMeshID(){ return m_meshID; }
	std::shared_ptr<Texture2D> getTexture(){ return m_texture; }
	GLuint getIndexCount(){ return m_indexCount; }

	// updates for the stored mesh and texture
	void updateMeshID(GLuint meshID){ m_meshID = meshID; }
	void updateTextureID(std::shared_ptr<Texture2D> texture){ m_texture = texture; }
	void updateIndexCount(GLuint indexCount){ m_indexCount = indexCount; }
	
private:
	GLuint m_meshID;
	GLuint m_indexCount;
	std::shared_ptr<Texture2D> m_texture;


};
