#pragma once

#include <GL\glew.h>
#include "../Component.h"

struct lightStruct {
	GLfloat diffuse[4];
	GLfloat specular[4];
};

// Basic Light class, it should be able to be used as a component as its fairly simple. Currently
// doesn't use ambient as deferred renderer is yet to support ambient.
// You can scale the lights radius, move its position and rotate it via the WorldPlacement Component
// the exact same as you'd do with a normal model, the rotation currently doesn't effect it as spotlights are unsupported for now
class LightComponent : public Component
{
public:
	// lightVolume is the Mesh you wish to use too encompass the area your lights effective range
	// using a sphere right now, doesn't support cones etc. yet, but feel free to give different meshes a shot
	// rest of inputs are as specified
	LightComponent(){ }
	LightComponent(unsigned int componentID, float diffuseR, float diffuseG, float diffuseB, float diffuseA, float specularR, float specularG, float specularB, float specularA);
	LightComponent(unsigned int componentID){ m_componentID = componentID; m_type = "LIGHT"; }
	virtual ~LightComponent(){ }

	lightStruct getLight(){ return m_light; }

	// update the diffuse and specular colours
	void updateLightDiffuse(float r, float g, float b, float a){ m_light.diffuse[0] = r, m_light.diffuse[1] = g, m_light.diffuse[2] = b, m_light.diffuse[3] = a; }
	void updateLightSpecular(float r, float g, float b, float a){ m_light.specular[0] = r, m_light.specular[1] = g, m_light.specular[2] = b, m_light.specular[3] = a; }

	
private:
	lightStruct m_light;

};
