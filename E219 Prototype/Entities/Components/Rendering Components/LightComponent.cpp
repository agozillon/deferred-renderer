#include "LightComponent.h"

LightComponent::LightComponent(unsigned int componentID, float diffuseR, float diffuseG, float diffuseB, float diffuseA, float specularR, float specularG, float specularB, float specularA) 
{
	m_light.diffuse[0] = diffuseR, m_light.diffuse[1] = diffuseG, m_light.diffuse[2] = diffuseB, m_light.diffuse[3] = diffuseA;
	m_light.specular[0] = specularR, m_light.specular[1] = specularG, m_light.specular[2] = specularB, m_light.specular[3] = specularA;
	
	m_componentID = componentID;
	m_type = "LIGHT";
}