//
//  ComponentManager.h
//  E219 Prototype
//
//  Created by Marty on 09/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#pragma once

#include <vector>
#include <memory>

#include "Component.h"
#include "World Placement Components\3D\WorldPlacement3DComponent.h"
#include "World Placement Components\2D\WorldPlacement2DComponent.h"
#include "Rendering Components\LightComponent.h"
#include "Rendering Components\ModelComponent.h"

class ComponentManager
{
    /* Class Constructors & Destructors */
public:
	ComponentManager() { };
    ComponentManager(unsigned int maximumComponents);
    ~ComponentManager();
    
    /* Component Management */
public:
	unsigned int orderComponent(const char* componentType);
	void assignComponent(const char* componentType, unsigned int componentIndex);
	void unassignComponent(const char* componentType, unsigned int componentIndex);
	void setComponentOwner(const char* componentType, unsigned int componentIndex, const char* entityName, unsigned int entityID);
	
	WorldPlacement2DComponent* getWorldPlacement2DComponent(unsigned int componentIndex);
	WorldPlacement3DComponent* getWorldPlacement3DComponent(unsigned int componentIndex);
	ModelComponent* getModelComponent(unsigned int componentIndex);
	LightComponent* getLightComponent(unsigned int componentIndex);

private:
	unsigned int getUnassignedWorldPlacement2DComponent();
	unsigned int getUnassignedWorldPlacement3DComponent();
	unsigned int getUnassignedLightComponent();
	unsigned int getUnassignedModelComponent();

	std::vector<WorldPlacement3DComponent> m_worldPlacement3DComponentVector;
	std::vector<WorldPlacement2DComponent> 	m_worldPlacement2DComponentVector;
	std::vector<LightComponent> m_lightComponentVector;
	std::vector<ModelComponent> m_modelComponentVector;

};