//
//  Component.cpp
//  E219 Prototype
//
//  Created by Marty on 07/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#include "Component.h"

#include "../Entity.h"

#pragma region Class Constructor & Destructor
/*
 * Constructs the Component class.
 */
Component::Component()
{
    m_assigned = false;
	m_entityOwner.first = "";
	m_entityOwner.second = 0;
}

/*
 * Destructs the Component class.
 */
Component::~Component()
{
    
}

#pragma endregion

#pragma region Component Management
/*
 * Get the ID for the Component.
 *
 * @return unsigned int - the integer identifier held by the Component.
 */
unsigned int Component::getComponentID()
{
    return m_componentID;
}

/*
 * Returns true or false if the Component has been assigned to an Entity.
 *
 * @return bool - the boolean to signify assignement.
 */
bool Component::isAssigned()
{
    return m_assigned;
}

/*
 * Sets assignment to true if Component has been assigned to an Entity.
 */
void Component::assign()
{
    m_assigned = true;
}

/*
 * Sets assignment to false if Component has been unassigned from an Entity.
 */
void Component::unassign()
{
    m_assigned = false;
}

/*
 * Gets the type of Component.
 *
 * @return const char* - string containing the type of Component.
 */
const char* Component::getType()
{
    return m_type;
}

#pragma endregion

#pragma region Entity Management
/*
 * Set the Component to point to its Entity owner.
 *
 * @param const char* entityName - string representing the assigned name of the Entity.
 * @param unsigned int entityID - the unique ID of the Entity.
 */
void Component::setOwner(const char* entityName, unsigned int entityID)
{
	m_entityOwner.first = entityName;
	m_entityOwner.second = entityID;
    
	if (m_entityOwner.first == nullptr)
    {
		std::cout << std::endl << "Component (Type: " << m_type << ", ID: " << m_componentID << ")'s owner set to Entity: (ID: " << m_entityOwner.second << ")." << std::endl;
    }
    else
    {
		std::cout << std::endl << "Component (Type: " << m_type << ", ID: " << m_componentID << ")'s owner set to Entity: (Name: " << m_entityOwner.first << ", ID: " << m_entityOwner.second << ")." << std::endl;
    }
}

/*
* Clear the owner of this Component. Used when unassigning the Component from an Entity.
*/
void Component::clearOwner()
{
	m_entityOwner.first = "";
	m_entityOwner.second = 0;
}

#pragma endregion