//
//  Component.h
//  E219 Prototype
//
//  Created by Marty on 07/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#pragma once

#include <memory>
#include <iostream>
#include <utility>

class Entity;

class Component
{
    /* Class Constructor & Destructor */
public:
	Component();
    virtual ~Component();
    
    /* Component Management */
public:
    virtual unsigned int getComponentID();
    virtual bool isAssigned();
    virtual void assign();
    virtual void unassign();
    virtual const char* getType();

protected:
    unsigned int m_componentID;
    bool m_assigned;
    const char* m_type;
    
    /* Entity Management */
public:
    void setOwner(const char* entityName, unsigned int entityID);
	void clearOwner();
    
protected:
	std::pair<const char*, unsigned int> m_entityOwner;
};