//
//  ComponentManager.cpp
//  E219 Prototype
//
//  Created by Marty on 09/07/2014.
//  Copyright (c) 2014 Martin Grant. All rights reserved.
//

#include "ComponentManager.h"

#pragma region Class Constructors & Destructors
/*
 * Constructs the ComponentManager class. Maximum Components works by settnig every component
 to have the passed in value available. 
 */
ComponentManager::ComponentManager(unsigned int maximumComponents)
{
	for (unsigned int index = 0; index < maximumComponents; ++index)
	{
		m_lightComponentVector.emplace_back(index);
		m_modelComponentVector.emplace_back(index);
		m_worldPlacement2DComponentVector.emplace_back(index);
		m_worldPlacement3DComponentVector.emplace_back(index);
	}    
}

/*
 * Destructs the ComponentManager class.
 */
ComponentManager::~ComponentManager()
{
    
}

#pragma endregion

#pragma region Component Management
/*
* Requests a new Component to be added to an Entity.
*
* @param const char* componentType - the type of Component.
* 
* @return unsigned int - the index of the Component that has been ordered.
*/
unsigned int ComponentManager::orderComponent(const char* componentType)
{
	unsigned int componentID = 0;

	// 2D Position/rotation/scalar all come from the same pool to make things 
	// easier (no need for 3 classes that are essentially just containers that do the same thing)
	if (componentType == "POSITION2D" || componentType == "ROTATION2D" 
		|| componentType == "SCALAR2D")
		componentID = getUnassignedWorldPlacement2DComponent();
	
	// Same as above except with 3D Positions/rotations/scalars
	if (componentType == "POSITION3D" || componentType == "ROTATION3D"
		|| componentType == "SCALAR3D")
		componentID = getUnassignedWorldPlacement3DComponent();
	
	// A Light component for the Deferred Renderer(Phong Shading Values),
	//should work for any 3D System except that the current Light Component also owns an id value for a 
	// 3D Model (a Deferred Light Volume) 
	if (componentType == "LIGHT")
		componentID = getUnassignedLightComponent();

	// Can be used as a 2D tile, 3D Model or GUI HUD element (just a 2D tile...)
	// All it holds is a 3D Model(or a quad etc. for 2D/GUI) and a texture
	if (componentType == "MODEL")
		componentID = getUnassignedModelComponent();

	return componentID;
}

/*
* Assign a Component to mark it as now being used by an Entity.
*
* @param const char* componentType - the type of Component, to determine which container to use.
* @param unsigned int componentIndex - the index of the Component from its container.
*/
void ComponentManager::assignComponent(const char* componentType, unsigned int componentIndex)
{
	if (componentType == "POSITION2D" || componentType == "ROTATION2D"
		|| componentType == "SCALAR2D")
	{
		m_worldPlacement2DComponentVector[componentIndex].setType(componentType);
		m_worldPlacement2DComponentVector[componentIndex].assign();
	}

	// Same as above except with 3D Positions/rotations/scalars
	if (componentType == "POSITION3D" || componentType == "ROTATION3D"
		|| componentType == "SCALAR3D")
	{
		m_worldPlacement3DComponentVector[componentIndex].setType(componentType);
		m_worldPlacement3DComponentVector[componentIndex].assign();
	}

	if (componentType == "LIGHT")
		m_lightComponentVector[componentIndex].assign();

	if (componentType == "MODEL")
		m_modelComponentVector[componentIndex].assign();
}

WorldPlacement3DComponent* ComponentManager::getWorldPlacement3DComponent(unsigned int componentIndex)
{
	return &m_worldPlacement3DComponentVector[componentIndex];
}

WorldPlacement2DComponent* ComponentManager::getWorldPlacement2DComponent(unsigned int componentIndex)
{
	return &m_worldPlacement2DComponentVector[componentIndex];
}

ModelComponent* ComponentManager::getModelComponent(unsigned int componentIndex)
{
	return &m_modelComponentVector[componentIndex];
}

LightComponent* ComponentManager::getLightComponent(unsigned int componentIndex)
{
	return &m_lightComponentVector[componentIndex];
}


/*
* Unassign a Component to mark it as not being used by an Entity.
*
* @param const char* componentType - the type of Component, to determine which container to use.
* @param unsigned int componentIndex - the index of the Component from its container.
*/
void ComponentManager::unassignComponent(const char* componentType, unsigned int componentIndex)
{
	if (componentType == "POSITION2D" || componentType == "ROTATION2D"
		|| componentType == "SCALAR2D")
	{
		m_worldPlacement2DComponentVector[componentIndex].unassign();
		m_worldPlacement2DComponentVector[componentIndex].clearOwner();
	}

	if (componentType == "POSITION3D" || componentType == "ROTATION3D"
		|| componentType == "SCALAR3D")
	{
		m_worldPlacement3DComponentVector[componentIndex].unassign();
		m_worldPlacement3DComponentVector[componentIndex].clearOwner();
	}

	if (componentType == "LIGHT")
	{
		m_lightComponentVector[componentIndex].unassign();
		m_lightComponentVector[componentIndex].clearOwner();
	}

	if (componentType == "MODEL")
	{
		m_modelComponentVector[componentIndex].unassign();
		m_modelComponentVector[componentIndex].clearOwner();
	}
}

/*
* Sets the owner of a Component to a specified Entity.
*
* @param const char* componentType - the type of Component to specify which vector the Component is contained in.
* @param unsigned int componentIndex - the index of the Component from its container.
* @param const char* entityName - the name of the owner Entity.
* @param unsigned int entityID - the unique ID of the owner Entity.
*/
void ComponentManager::setComponentOwner(const char* componentType, unsigned int componentIndex, const char* entityName, unsigned int entityID)
{
	if (componentType == "POSITION2D" || componentType == "ROTATION2D"
		|| componentType == "SCALAR2D")
	{
		m_worldPlacement2DComponentVector[componentIndex].setType(componentType);
		m_worldPlacement2DComponentVector[componentIndex].setOwner(entityName, entityID);
	}
	
	if (componentType == "POSITION3D" || componentType == "ROTATION3D"
		|| componentType == "SCALAR3D")
	{
		m_worldPlacement3DComponentVector[componentIndex].setType(componentType);
		m_worldPlacement3DComponentVector[componentIndex].setOwner(entityName, entityID);
	}
	
	if (componentType == "LIGHT")
		m_lightComponentVector[componentIndex].setOwner(entityName, entityID);

	if (componentType == "MODEL")
		m_modelComponentVector[componentIndex].setOwner(entityName, entityID);

}

#pragma region Get Unassigned Component Functions

unsigned int ComponentManager::getUnassignedWorldPlacement2DComponent()
{
	unsigned int component = 0;

	for (unsigned int index = 1; index < m_worldPlacement2DComponentVector.size(); ++index)
	{
		if (m_worldPlacement2DComponentVector[index].isAssigned() == false)
		{
			m_worldPlacement2DComponentVector[index].assign();
			component = index;
			break;
		}
	}

	return component;
}

unsigned int ComponentManager::getUnassignedWorldPlacement3DComponent()
{
	unsigned int component = 0;

	for (unsigned int index = 1; index < m_worldPlacement3DComponentVector.size(); ++index)
	{
		if (m_worldPlacement3DComponentVector[index].isAssigned() == false)
		{
			m_worldPlacement3DComponentVector[index].assign();
			component = index;
			break;
		}
	}

	return component;
}

unsigned int ComponentManager::getUnassignedLightComponent()
{
	unsigned int component = 0;

	for (unsigned int index = 1; index < m_lightComponentVector.size(); ++index)
	{
		if (m_lightComponentVector[index].isAssigned() == false)
		{
			m_lightComponentVector[index].assign();
			component = index;
			break;
		}
	}

	return component;
}

unsigned int ComponentManager::getUnassignedModelComponent()
{
	unsigned int component = 0;

	for (unsigned int index = 1; index < m_modelComponentVector.size(); ++index)
	{
		if (m_modelComponentVector[index].isAssigned() == false)
		{
			m_lightComponentVector[index].assign();
			component = index;
			break;
		}
	}

	return component;
}

#pragma endregion 

#pragma endregion
