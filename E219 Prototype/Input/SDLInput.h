#pragma once

#include <utility>
#include <iostream>
#include <unordered_map>

#ifdef _WIN32
#include <SDL.h>
#elif __APPLE__
#include <SDL2/SDL.h>
#endif

#include "Input.h"
#include "SDLController.h"

class SDLInput : public Input
{
public:
	/* Class Constructor & Destructor */
	SDLInput(void);
	virtual ~SDLInput(void);

public:
	/* General Public Methods */
	virtual void update();

public:
	/* Keyboard Methods */
	virtual bool getKeyState(const char* key);

public:
	/* Mouse Methods */
	virtual std::pair<int, int> getMousePosition();
	virtual std::pair<int, int> getMouseRelativePosition();
	virtual bool mouseButtonState(unsigned int buttonID);
	virtual int getMouseWheel();

public:
	/* Controller Methods */
	virtual int getControllerAxis(unsigned int controllerID, const char* axis);
	virtual bool getControllerButtonState(unsigned int controllerID, const char* button);
	virtual bool onControllerButtonUp(unsigned int controllerID, const char* button);
	virtual bool onControllerButtonDown(unsigned int controllerID, const char* button);

private:
	/* Controller Methods */
	int detectControllers();
	void detectHaptics();
	bool openController(unsigned int controllerID);
	bool closeController(unsigned int controllerID);
	void pollControllers();

private:
	SDL_Event m_SDLEvent;
	int m_connectedControllers;
	std::unordered_map<int, SDLController> m_controllerList;
};

