#include "DeferredRenderer.h"
#include <iostream>

DeferredRenderer::DeferredRenderer(std::shared_ptr<ShaderManager> shaderManager, unsigned int windowWidth, unsigned int windowHeight)
{
	m_windowWidth = windowWidth;
	m_windowHeight = windowHeight;
	m_shaderManager = shaderManager;

	initShaderPrograms();
	initGBuffer();
}

// Geometry pass call this and then render all of your scenes geometry (that you want rendered with the deferred renderer)
// essentially adds all there relevant data to the gbuffer, position, depth, normals and diffuse colour right now.
void DeferredRenderer::setGeometryPass()
{
	// clearing the main default FBO now a new pass has begun
	// don't clear it in the stencil or light phase as Stencil has nothing to do 
	// with the default FBO and the lighting stage is additive!
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT); 

	glBindFramebuffer(GL_FRAMEBUFFER, m_GBuffer); // binding the gbuffer for rendering too
	
	// disabling depth mask disables writing to the depth buffer, in any way including clearing
	// so has to be specified before
	glEnable(GL_DEPTH_TEST); // we want depth testing since we're rendering all our geometry!
	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LESS);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glDisable(GL_BLEND); // don't want to blend colours together 
	
	// stencil related
	glCullFace(GL_BACK);
	glDisable(GL_STENCIL_TEST);
	
	m_shaderManager->getShaderProgram("deferredFirstPassShader")->use();
}

void DeferredRenderer::endGeometryPass()
{
	// blitting all the ambient information across
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, m_GBuffer);
	glReadBuffer(GL_COLOR_ATTACHMENT3);
	glBlitFramebuffer(0, 0, m_windowWidth, m_windowHeight, 0, 0, m_windowWidth, m_windowHeight, GL_COLOR_BUFFER_BIT, GL_NEAREST);
}

// Stencil pass call this and render light volumes, which will then populate the Stencil buffer with data
// that allows the deferred renderer to tell whats in our outside of a light on the Z axis (prior to stencil
// buffering it will only occlude things on the x/y of the camera that are outside of the volume)
void DeferredRenderer::setStencilPass()
{

	glBindFramebuffer(GL_FRAMEBUFFER, m_GBuffer);

	// Doing the stencil test on the light volumes, making sure 
	// we only write colour to the pixels that are inside the lights z axis (camera perspective)
	glEnable(GL_STENCIL_TEST);  // enable stencil test
	glEnable(GL_DEPTH_TEST); // wish to continue using the depth test for our stencil operations!
	glDepthMask(GL_FALSE); // disable writing to the depth mask, don't want the light volumes depth want to keep the world objects!
	glDisable(GL_CULL_FACE); // ain't want no sphere culling! need to test both sides for the stencil pass
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE); // disable colour write, don't need it or want it to write to our textures
	glClear(GL_STENCIL_BUFFER_BIT); // clean it for the new pass

	// for both back faces and front faces if it stencil fails or depth passes
	// keep the stencil value the same, HOWEVER if it fails the depth test increment it on
	// failure for back facing polygons and decrement it for front facing polygons.
	// Essentially everything infront of the light bounding sphere will fail the depth test
	// twice on both sides of the polygon making it decrement -1 and increment +1 thus coming out 
	// as a 0 on the stencil buffer, anything that is behind the sphere passes the depth test
	// on all counts and ends up 0 again (no incrementation or decrementation). Anything within
	// a bounding sphere will pass the GL_FRONT stencil op and have no decrementation but fail the
	// GL_BACK op and get incremented! So an object inside the sphere will get +1(or more I.E Multiple spheres)
	// on the stencil test and any objects outside will get 0. We now know exactly what pixels are inside the
	// light volumes and that we should render colour to these pixels.
	// OF NOTE: this is from the cameras perspective, the depth tests are based on the light volume being rendered into the scene
	// I.E if an object is infront of the light volume it fails GL_FRONT as the light source is not LEQUAL to the object, so we're
	// testing the light volume depth against everything for the tests not the other way around!
	glStencilFunc(GL_ALWAYS, 0, 0); // always pass the stencil test and leave the values as is, only care about the depth test just need stencil to store values
	glStencilOpSeparate(GL_BACK, GL_KEEP, GL_INCR_WRAP, GL_KEEP);
	glStencilOpSeparate(GL_FRONT, GL_KEEP, GL_DECR_WRAP, GL_KEEP);

	m_shaderManager->getShaderProgram("deferredStencilPassShader")->use();
}

// Light pass, this is the pass where you render your light volume and all of the colour gets calculated 
// for objects within the light volume. 
void DeferredRenderer::setLightPass()
{
	// Rendering the lights into scene, this is the pass where all the lighting happens!
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE); // enable colour write, we need it

	// blitting(copying) the depth and stencil buffer over to the default FBO so we can perform our 
	// lighting calculations and use our stencil buffer to cut out objects infront and behind our light 
	// volumes range
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, m_GBuffer);
	glBlitFramebuffer(0, 0, m_windowWidth, m_windowHeight, 0, 0, m_windowWidth, m_windowHeight, GL_STENCIL_BUFFER_BIT, GL_NEAREST);
	glBlitFramebuffer(0, 0, m_windowWidth, m_windowHeight, 0, 0, m_windowWidth, m_windowHeight, GL_DEPTH_BUFFER_BIT, GL_NEAREST);

	// Blend additively this way when we render the lights+textures together
	// they get layered on and areas where there are no color/objects get ignored
	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_ONE, GL_ONE);
	// Don't want to depth test, unneeded for this stage
	glDepthMask(GL_FALSE);
	glDisable(GL_DEPTH_TEST);
	
	// 0xFF essentially disables the stencil mask that gets applied to the Stencil value 
	// and reference value before checking, GL_NOTEQUAL to 0 so render if not equal to 0
	glStencilFunc(GL_NOTEQUAL, 0, 0xFF); 				
	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);

	// diffuse, position, normal g-buffer bound
	m_GBufferTextures[2]->bindTexture(GL_TEXTURE2);
	m_GBufferTextures[1]->bindTexture(GL_TEXTURE1);
	m_GBufferTextures[0]->bindTexture(GL_TEXTURE0);

	m_shaderManager->getShaderProgram("deferredSecondPassShader")->use(); // second pass shader set
}

// Renders the gbuffer to screen for debug purposes, this will overwrite anything that's currently displayed on screen
// as it copies all of the gbuffer FBO colour data over to the default framebuffer using glBlit (1/4 of the screen for each)
// doesn't render the depth/stencil buffer as GL dislikes displaying this without binding it as a texture to an object apparently.
void DeferredRenderer::renderGbufferDebug()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, m_GBuffer);

	// positions
	glReadBuffer(GL_COLOR_ATTACHMENT0);
	glBlitFramebuffer(0, 0, m_windowWidth, m_windowHeight, 0, 0, m_windowWidth / 2, m_windowHeight / 2, GL_COLOR_BUFFER_BIT, GL_NEAREST);

	// normals
	glReadBuffer(GL_COLOR_ATTACHMENT1);
	glBlitFramebuffer(0, 0, m_windowWidth, m_windowHeight, m_windowWidth / 2, 0, m_windowWidth, m_windowHeight / 2, GL_COLOR_BUFFER_BIT, GL_NEAREST);

	// diffuse texture
	glReadBuffer(GL_COLOR_ATTACHMENT2);
	glBlitFramebuffer(0, 0, m_windowWidth, m_windowHeight, 0, m_windowHeight / 2, m_windowWidth / 2, m_windowHeight, GL_COLOR_BUFFER_BIT, GL_NEAREST);

	// ambient
	glReadBuffer(GL_COLOR_ATTACHMENT3);
	glBlitFramebuffer(0, 0, m_windowWidth, m_windowHeight, m_windowWidth / 2, m_windowHeight / 2, m_windowWidth, m_windowHeight, GL_COLOR_BUFFER_BIT, GL_NEAREST);
	
}

// Initializes the G-Buffer, Textures and the FBO setup
void DeferredRenderer::initGBuffer()
{

	// 0 = position
	m_GBufferTextures[0] = std::shared_ptr<Texture2D>(new Texture2D(GL_TEXTURE_2D, m_windowWidth, m_windowHeight, GL_RGBA32F, GL_RGBA, GL_UNSIGNED_BYTE, NULL)); 
	m_GBufferTextures[0]->bindTexture(GL_TEXTURE0);
	m_GBufferTextures[0]->updateMinFilter(GL_NEAREST);
	m_GBufferTextures[0]->updateMagFilter(GL_NEAREST);
	m_GBufferTextures[0]->updateSWrap(GL_CLAMP_TO_EDGE);
	m_GBufferTextures[0]->updateTWrap(GL_CLAMP_TO_EDGE);

	// 1 = normals
	m_GBufferTextures[1] = std::shared_ptr<Texture2D>(new Texture2D(GL_TEXTURE_2D, m_windowWidth, m_windowHeight, GL_RGBA16F, GL_RGBA, GL_UNSIGNED_BYTE, NULL));
	m_GBufferTextures[1]->bindTexture(GL_TEXTURE0);
	m_GBufferTextures[1]->updateMinFilter(GL_NEAREST);
	m_GBufferTextures[1]->updateMagFilter(GL_NEAREST);
	m_GBufferTextures[1]->updateSWrap(GL_CLAMP_TO_EDGE);
	m_GBufferTextures[1]->updateTWrap(GL_CLAMP_TO_EDGE);

	// 2 = diffuse
	m_GBufferTextures[2] = std::shared_ptr<Texture2D>(new Texture2D(GL_TEXTURE_2D, m_windowWidth, m_windowHeight, GL_RGBA16F, GL_RGBA, GL_UNSIGNED_BYTE, NULL));
	m_GBufferTextures[2]->bindTexture(GL_TEXTURE0);
	m_GBufferTextures[2]->updateMinFilter(GL_NEAREST);
	m_GBufferTextures[2]->updateMagFilter(GL_NEAREST);
	m_GBufferTextures[2]->updateSWrap(GL_CLAMP_TO_EDGE);
	m_GBufferTextures[2]->updateTWrap(GL_CLAMP_TO_EDGE);

	// 3 = ambient and possible future "accumulation" buffer
	m_GBufferTextures[3] = std::shared_ptr<Texture2D>(new Texture2D(GL_TEXTURE_2D, m_windowWidth, m_windowHeight, GL_RGBA16F, GL_RGBA, GL_UNSIGNED_BYTE, NULL));
	m_GBufferTextures[3]->bindTexture(GL_TEXTURE0);
	m_GBufferTextures[3]->updateMinFilter(GL_NEAREST);
	m_GBufferTextures[3]->updateMagFilter(GL_NEAREST);
	m_GBufferTextures[3]->updateSWrap(GL_CLAMP_TO_EDGE);
	m_GBufferTextures[3]->updateTWrap(GL_CLAMP_TO_EDGE);

	// 4 = depth & stencil
	m_GBufferTextures[4] = std::shared_ptr<Texture2D>(new Texture2D(GL_TEXTURE_2D, m_windowWidth, m_windowHeight, GL_DEPTH24_STENCIL8, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, NULL));
	m_GBufferTextures[4]->bindTexture(GL_TEXTURE0);
	m_GBufferTextures[4]->updateMinFilter(GL_NEAREST);
	m_GBufferTextures[4]->updateMagFilter(GL_NEAREST);
	m_GBufferTextures[4]->updateSWrap(GL_CLAMP_TO_EDGE);
	m_GBufferTextures[4]->updateTWrap(GL_CLAMP_TO_EDGE);

	// generate framebuffer, bind and attach all of the relevant textures to our Gbuffer!
	//type of Buffer Mode it is could be GL_DEPTH_ATTACHMENT, GL_DEPTH_STENCIL_ATTACHMENT, GL_COLOR_ATTACHMENTi etc
	glGenFramebuffers(1, &m_GBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, m_GBuffer);
	glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_GBufferTextures[0]->getTextureID(), 0);
	glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, m_GBufferTextures[1]->getTextureID(), 0);
	glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, m_GBufferTextures[2]->getTextureID(), 0);
	glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, m_GBufferTextures[3]->getTextureID(), 0);
	glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, m_GBufferTextures[4]->getTextureID(), 0);

	// setting the DrawBufferMode/what i want sent to this buffers attachments
	GLenum buffers[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3 };
	glDrawBuffers(4, buffers);
	
#if _DEBUG
	// simply checking if the framebuffer completed fine or if it has an error 
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "framebuffer error" << std::endl;
#endif

	// unbinding current buffer, should go back to default
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

}

// Sets all the GL APIs components back to the normal 3D forward rendering setup
void DeferredRenderer::setDefaults()
{
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE); // enable colour write, we need it
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_STENCIL_TEST);
	glEnable(GL_BLEND);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_TRUE);
}

void DeferredRenderer::initShaderPrograms()
{
	///////////////////////////////////
	/////DEFERRED PROGRAM FIRST PASS///
	///////////////////////////////////
	m_shaderManager->createShaderProgram("deferredFirstPassShader");

	// respective to the mesh binding
	m_shaderManager->addShaderProgramAttribute("deferredFirstPassShader", 0, "in_Position");
	m_shaderManager->addShaderProgramAttribute("deferredFirstPassShader", 1, "in_TexCoord");
	m_shaderManager->addShaderProgramAttribute("deferredFirstPassShader", 2, "in_Normal");

	// position goes to 0, normals go to 1, colour goes to 2, respective to the framebuffer attachements
	m_shaderManager->addShaderFragOutput("deferredFirstPassShader", 0, "out_Position");
	m_shaderManager->addShaderFragOutput("deferredFirstPassShader", 1, "out_Normal");
	m_shaderManager->addShaderFragOutput("deferredFirstPassShader", 2, "out_Color");
	m_shaderManager->addShaderFragOutput("deferredFirstPassShader", 3, "out_Ambient");

	m_shaderManager->attachShaderToProgram("deferredFirstPassShader", "deferredFirstPassVert");
	m_shaderManager->attachShaderToProgram("deferredFirstPassShader", "deferredFirstPassFrag");
	m_shaderManager->compileShaderProgram("deferredFirstPassShader");

	//////////////////////////////////////////////////
	/////DEFERRED PROGRAM SECOND PASS/////////////////
	//////////////////////////////////////////////////
	m_shaderManager->createShaderProgram("deferredSecondPassShader");

	m_shaderManager->addShaderProgramAttribute("deferredSecondPassShader", 0, "in_Position");

	m_shaderManager->addShaderFragOutput("deferredSecondPassShader", 0, "out_Color");

	m_shaderManager->attachShaderToProgram("deferredSecondPassShader", "mvp");
	m_shaderManager->attachShaderToProgram("deferredSecondPassShader", "deferredSecondPassFrag");
	m_shaderManager->compileShaderProgram("deferredSecondPassShader");

	m_shaderManager->getShaderProgram("deferredSecondPassShader")->use();
	// sending screen resolution (only needs to be sent once or any time the screen size gets changed)
	m_shaderManager->getShaderProgram("deferredSecondPassShader")->setUniform2fv("screenResolution", 1, glm::value_ptr(glm::vec2(m_windowWidth, m_windowHeight)));

	// setting the texture identifiers in the shader, so the g-buffer stuff 99.9% of the
	// time gets bound and sent to the right location all the time (4.4 has a cool in-built way to do this like location specifiers)
	m_shaderManager->getShaderProgram("deferredSecondPassShader")->setUniformi("positionTexture", 0);
	m_shaderManager->getShaderProgram("deferredSecondPassShader")->setUniformi("normalTexture", 1);
	m_shaderManager->getShaderProgram("deferredSecondPassShader")->setUniformi("diffuseTexture", 2);


	//////////////////////////////////////////////////
	/////DEFERRED STENCIL PASS PROGRAM////////////////
	//////////////////////////////////////////////////
	m_shaderManager->createShaderProgram("deferredStencilPassShader");
	m_shaderManager->addShaderProgramAttribute("deferredStencilPassShader", 0, "in_Position");
	m_shaderManager->attachShaderToProgram("deferredStencilPassShader", "mvp");
	m_shaderManager->attachShaderToProgram("deferredStencilPassShader", "null");
	m_shaderManager->compileShaderProgram("deferredStencilPassShader");
}
