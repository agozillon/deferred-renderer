#ifndef DEFERREDRENDERER_H
#define DEFERREDRENDERER_H
#include <GL\glew.h>
#include <memory.h>
#include "../../Shader Manager/ShaderManager.h"
#include "../Texture/Texture2D.h"

// HOW TO: Incase your using this outside of the RenderManager for whatever reason this is how you'd use
// the DeferredRenderer, 
// Step 1: Initialize the DeferredRenderer, 
// Step 2: call setGeometryPass when ready to render your scenes geometry, after the call render all the geometry
// like you normally would in forward rendering, I.E bind textures and send uniforms to the relevant shader(which's
// name can be found within the GeometryPass function)
// Step 3 & 4 are an iterating sequence you'll do it one after another until all your light volumes(sphere mesh or whatever
// your currently using to occlude light outside of your lights area), so you'll do step 3, render light volume 1, step 4, 
// render light volume 1, step 3 render light volume 2.....etc.
// Step 3: call setStencilPass, render your light volume same as you'd render any other mesh just sending the correct uniforms
// to the shader thats bound in the setStencilPass function.
// Step 4: call setLightPass, do pretty much the same as step 3 except with the shader specified in light pass.

// DeferredRenderer encapsulates the gbuffer and various setup functions that need to be called before that
// specific deferred rendering stage takes place, geometry pass, stencil pass and light pass...also 
// contains a function to set the rendering back to "default" 3D forward rendering. 
class DeferredRenderer
{
public:
	DeferredRenderer(std::shared_ptr<ShaderManager>, unsigned int windowWidth, unsigned int windowHeight);
	~DeferredRenderer(){}

	void setGeometryPass();     // binds all the relevant GL states, functions and shader..etc needed for the geometry pass
	void endGeometryPass();     // ends the Geometry Pass, changing all Geometry specific GL states back
	void setStencilPass();      // binds all the relevant GL states, functions and shader..etc needed for the stencil pass
	void setLightPass();	    // binds all the relevant GL states, functions and shader..etc needed for the light pass
	void renderGbufferDebug();  // renders all of the gbuffer data except the depth/stencil texture onto the screen when called
	void setDefaults();			// sets all of the OpenGL render settings back to "Default" forward rendering mode

	// Get functions for respective textures and framebuffers
	std::shared_ptr<Texture2D> getPositionTexture(){ return m_GBufferTextures[0];}
	std::shared_ptr<Texture2D> getNormalTexture(){ return m_GBufferTextures[1];}
	std::shared_ptr<Texture2D> getDiffuseColorTexture(){ return m_GBufferTextures[2]; }
	std::shared_ptr<Texture2D> getStencilDepthTexture(){ return m_GBufferTextures[3]; }
	GLuint getGBufferFBO(){ return m_GBuffer;}

private:
	void initGBuffer();
	void initShaderPrograms();

	// temp function
	GLuint createTexture(GLuint textureW, GLuint textureH, GLenum internalFormat, GLenum dataFormat, GLenum dataType, GLenum minFilterType, GLenum magFilterType, GLenum clampType, GLenum activeTexture, GLvoid* data);
	
	std::shared_ptr<Texture2D> m_GBufferTextures[5];
	GLuint m_GBuffer;
	unsigned int m_windowWidth;
	unsigned int m_windowHeight;

	std::shared_ptr<ShaderManager> m_shaderManager;
};

#endif