#ifndef RENDERMANAGER_H
#define RENDERMANAGER_H
#include <memory.h>
#include <vector>
#include "../../Camera Manager/CameraManager.h"
#include "../../Entities/EntityManager.h"
#include "../../Shader Manager/ShaderManager.h"
#include "../../Renderer/Deferred Renderer/DeferredRenderer.h"

// Simple RenderManager at the moment, only handles Deferred Rendering for the moment
// A lot of it will have to be changed on introduction of the proper components system,
// mainly internal changes, rather than API changes
class RenderManager
{
public:
	RenderManager(unsigned int windowWidth, unsigned int windowHeight, std::shared_ptr<ShaderManager> shaderManager, std::shared_ptr<CameraManager> cameraManager);
	~RenderManager(){};

	void render(std::shared_ptr<EntityManager> entities);
	void updateWindowWidth(unsigned width){ m_windowWidth = width; }
	void updateWindowHeight(unsigned height){ m_windowHeight = height; }

private:
	std::shared_ptr<ShaderManager> m_shaderManager;
	std::shared_ptr<CameraManager> m_cameraManager;
	std::unique_ptr<DeferredRenderer> m_deferredRenderer;
	

	unsigned int m_windowWidth;
	unsigned int m_windowHeight;
};

#endif