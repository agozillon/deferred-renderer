#include "RenderManager.h"
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp> 
#include <stack>
#include <iostream>
#include <string>
#include "../../Entities/Components/Rendering Components/LightComponent.h"
#include "../../Entities/Components/Rendering Components/ModelComponent.h"
#include "../../Entities/Components/World Placement Components/3D/WorldPlacement3DComponent.h"

RenderManager::RenderManager(unsigned int windowWidth, unsigned int windowHeight, std::shared_ptr<ShaderManager> shaderManager, std::shared_ptr<CameraManager> cameraManager)
: m_windowWidth(windowWidth), m_windowHeight(windowHeight), m_shaderManager(shaderManager), m_cameraManager(cameraManager)
{
	m_deferredRenderer = std::unique_ptr<DeferredRenderer>(new DeferredRenderer(m_shaderManager, m_windowWidth, m_windowHeight));
}

void RenderManager::render(std::shared_ptr<EntityManager> entities)
{
	std::stack<glm::mat4> tmp;
	tmp.push(glm::mat4(1.0));

	// all these passes are to do with geometry
	m_deferredRenderer->setGeometryPass();
	
	std::string entityNameSearch = "MODEL";
	
	for (size_t i = 1; i < entities->getAllocatedEntityCount(); ++i)
	{
		if (entityNameSearch.compare(0, std::string::npos, entities->getEntityReference(i).getEntityName(), 5) == 0)
		{
				tmp.push(glm::translate(tmp.top(), glm::vec3(entities->getWorldPlacement3DComponent(i, "POSITION3D")->getWorld3DPlacementX(),
				  entities->getWorldPlacement3DComponent(i, "POSITION3D")->getWorld3DPlacementY(), entities->getWorldPlacement3DComponent(i, "POSITION3D")->getWorld3DPlacementZ())));

				tmp.top() = glm::rotate(tmp.top(), entities->getWorldPlacement3DComponent(i, "ROTATION3D")->getWorld3DPlacementX(), glm::vec3(1, 0, 0));
				tmp.top() = glm::rotate(tmp.top(), entities->getWorldPlacement3DComponent(i, "ROTATION3D")->getWorld3DPlacementY(), glm::vec3(0, 1, 0));
				tmp.top() = glm::rotate(tmp.top(), entities->getWorldPlacement3DComponent(i, "ROTATION3D")->getWorld3DPlacementZ(), glm::vec3(0, 0, 1));
			
				tmp.top() = glm::scale(tmp.top(), glm::vec3(entities->getWorldPlacement3DComponent(i, "SCALAR3D")->getWorld3DPlacementX(),
					entities->getWorldPlacement3DComponent(i, "SCALAR3D")->getWorld3DPlacementY(), entities->getWorldPlacement3DComponent(i, "SCALAR3D")->getWorld3DPlacementZ()));

				m_shaderManager->getShaderProgram("deferredFirstPassShader")->setUniformMatrix4fv("model", 1, false, glm::value_ptr(tmp.top()));
				m_shaderManager->getShaderProgram("deferredFirstPassShader")->setUniformMatrix3fv("normalmatrix", 1, false, glm::value_ptr(glm::transpose(glm::inverse(glm::mat3x3(tmp.top())))));

				tmp.top() = m_cameraManager->getProjection() * m_cameraManager->getView() * tmp.top();
				m_shaderManager->getShaderProgram("deferredFirstPassShader")->setUniformMatrix4fv("MVP", 1, false, glm::value_ptr(tmp.top()));
				
				entities->getModelComponent(i)->getTexture()->bindTexture(GL_TEXTURE0);
				glBindVertexArray(entities->getModelComponent(i)->getMeshID());	// Bind mesh VAO
				glDrawElements(GL_TRIANGLES, entities->getModelComponent(i)->getIndexCount(), GL_UNSIGNED_INT, 0);	// draw VAO 
				entities->getModelComponent(i)->getTexture()->unbindTexture();
		
				tmp.pop();
				
		}
		
	}
	
	m_deferredRenderer->endGeometryPass();
	
	entityNameSearch = "LIGHT";

	for (size_t i = 1; i < entities->getAllocatedEntityCount(); ++i)
	{
		
		if (entityNameSearch.compare(0, 5, entities->getEntityReference(i).getEntityName(), 5) == 0)
		{
			
			m_deferredRenderer->setStencilPass();
			
			tmp.push(glm::translate(tmp.top(), glm::vec3(entities->getWorldPlacement3DComponent(i, "POSITION3D")->getWorld3DPlacementX(),
				entities->getWorldPlacement3DComponent(i, "POSITION3D")->getWorld3DPlacementY(), entities->getWorldPlacement3DComponent(i, "POSITION3D")->getWorld3DPlacementZ())));

			tmp.top() = glm::scale(tmp.top(), glm::vec3(entities->getWorldPlacement3DComponent(i, "SCALAR3D")->getWorld3DPlacementX(),
				entities->getWorldPlacement3DComponent(i, "SCALAR3D")->getWorld3DPlacementY(), entities->getWorldPlacement3DComponent(i, "SCALAR3D")->getWorld3DPlacementZ()));

			tmp.top() = m_cameraManager->getProjection() * m_cameraManager->getView() * tmp.top();
			
			m_shaderManager->getShaderProgram("deferredStencilPassShader")->setUniformMatrix4fv("MVP", 1, false, glm::value_ptr(tmp.top()));

			glBindVertexArray(entities->getModelComponent(i)->getMeshID());	// Bind mesh VAO
			glDrawElements(GL_TRIANGLES, entities->getModelComponent(i)->getIndexCount(), GL_UNSIGNED_INT, 0);	// draw VAO 
			
			m_deferredRenderer->setLightPass();
			
			m_shaderManager->getShaderProgram("deferredSecondPassShader")->setLight(entities->getLightComponent(i)->getLight());
			m_shaderManager->getShaderProgram("deferredSecondPassShader")->setUniform3fv("lightPosition", 1, glm::value_ptr(glm::vec3(entities->getWorldPlacement3DComponent(i, "POSITION3D")->getWorld3DPlacementX(),
				entities->getWorldPlacement3DComponent(i, "POSITION3D")->getWorld3DPlacementY(), entities->getWorldPlacement3DComponent(i, "POSITION3D")->getWorld3DPlacementZ())));
			m_shaderManager->getShaderProgram("deferredSecondPassShader")->setUniform3fv("cameraPos", 1, glm::value_ptr(m_cameraManager->getEye()));
			m_shaderManager->getShaderProgram("deferredSecondPassShader")->setUniformMatrix4fv("MVP", 1, false, glm::value_ptr(tmp.top()));

			glBindVertexArray(entities->getModelComponent(i)->getMeshID());	// Bind mesh VAO
			glDrawElements(GL_TRIANGLES, entities->getModelComponent(i)->getIndexCount(), GL_UNSIGNED_INT, 0);	// draw VAO 

			tmp.pop();
				
		}
	}
	
}