#pragma once

#include <memory>
#include <iostream>

#include "../Screens/ScreenManager.h"
#include "../Input/Input.h"
#include "../Audio/Audio.h"
#include "../TimerManager/TimerManager.h"

class ServiceManager
{
public:
	/* Class Constructors & Destructors */
	ServiceManager();
	~ServiceManager();

public:
	/* Service Management */
	void registerService(std::shared_ptr<ScreenManager> screenManager);
	std::shared_ptr<ScreenManager> getScreenManager();

	void registerService(std::shared_ptr<Input> inputService);
	std::shared_ptr<Input> getInputService();
	
	void registerService(std::shared_ptr<Audio> audioService);
	std::shared_ptr<Audio> getAudioService();

	void registerService(std::shared_ptr<TimerManager> timerService);
	std::shared_ptr<TimerManager> getTimerService();

private:
	/* Service Members */
	std::shared_ptr<ScreenManager> m_screenManager;
	std::shared_ptr<Input> m_inputService;
	std::shared_ptr<Audio> m_audioService;
	std::shared_ptr<TimerManager> m_timerService;
};

