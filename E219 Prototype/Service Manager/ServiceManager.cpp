#include "ServiceManager.h"


#pragma region Class Constructors & Destructors 
/*
 * Constructs the ServiceManager class.
 */
ServiceManager::ServiceManager()
{
	std::cout << "Constructed ServiceManager succesfully." << std::endl << std::endl;
}

/*
 * Destructs the ServiceManager class.
 */
ServiceManager::~ServiceManager()
{
	std::cout << "Destructed ServiceManager successfully." << std::endl << std::endl;
}

#pragma endregion


#pragma region Service Management
/*
 * Registers a new ScreenManager service with the ServiceManager.
 *
 * @param std::shared_ptr<ScreenManager> screenManager - shared pointer to a ScreenManager to be set as ServiceManager's ScreenManager
 */
void ServiceManager::registerService(std::shared_ptr<ScreenManager> screenManager)
{
	m_screenManager = screenManager;
}

/*
 * Returns ScreenManager
 *
 * @return std::shared_ptr<ScreenManager> m_screenManager - ScreenManager instance held in the ServiceManager.
 */
std::shared_ptr<ScreenManager> ServiceManager::getScreenManager()
{
	return m_screenManager;
}

/*
 * Registers a new Input service with the Service Manager.
 *
 * @param std::shared_ptr<Input> inputService - shared pointer to an Input instance to be set as ServiceManager's Input service.
 */
void ServiceManager::registerService(std::shared_ptr<Input> inputService)
{
	m_inputService = inputService;
}

/*
 * Returns Input service.
 *
 * @return std::shared_ptr<Input> m_inputService - shared pointer to an Input instance held by the ServiceManager.
 */
std::shared_ptr<Input> ServiceManager::getInputService()
{
	return m_inputService;
}

/*
* Registers a new Audio service with the Service Manager.
*
* @param std::shared_ptr<Audio> audioService - shared pointer to an Audio instance to be set as ServiceManager's Audio service.
*/
void ServiceManager::registerService(std::shared_ptr<Audio> audioService)
{
	m_audioService = audioService;
}

/*
* Returns Audio service.
*
* @return std::shared_ptr<Audio> m_audioService - shared pointer to an Audio instance held by the ServiceManager.
*/
std::shared_ptr<Audio> ServiceManager::getAudioService()
{
	return m_audioService;
}

/*
* Registers a new TimerManager service with the Service Manager.
*
* @param std::shared_ptr<TimerManager> timerService - shared pointer to an TimerManager instance to be set as ServiceManager's TimerManager service.
*/
void ServiceManager::registerService(std::shared_ptr<TimerManager> timerService)
{
	m_timerService = timerService;
}

/*
* Returns TimerManager service.
*
* @return std::shared_ptr<TimerManager> m_timerService - shared pointer to an TimerManager instance held by the ServiceManager.
*/
std::shared_ptr<TimerManager> ServiceManager::getTimerService()
{
	return m_timerService;
}

#pragma endregion
