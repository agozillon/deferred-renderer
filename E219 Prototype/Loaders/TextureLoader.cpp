#include "TextureLoader.h"

#ifdef _WIN32
#include <SDL_image.h>
#elif __APPLE__
#include <SDL2_image/SDL_image.h>
#endif

#include <iostream>

// Basic Texture loading function slightly modified version we're all used too (Litreally one function call
// and some added .dlls/links for multiple texture type support)
std::shared_ptr<Texture2D> TextureLoader::loadTexture(char *fname) 
{
	GLuint texID;
	glGenTextures(1, &texID); // generate texture ID
	// load file - using core SDL library
	SDL_Surface *tmpSurface;
    tmpSurface = IMG_Load(fname);
	if (!tmpSurface)
	{
		std::cout << "Error loading texture" << std::endl;
	}

	SDL_PixelFormat *format = tmpSurface->format;

	GLuint externalFormat, internalFormat;
	if (format->Amask) 
	{
		internalFormat = GL_RGBA;
		externalFormat = (format->Rmask < format->Bmask) ? GL_RGBA : GL_BGRA;
	}
	else 
	{
		internalFormat = GL_RGB;
		externalFormat = (format->Rmask < format->Bmask) ? GL_RGB : GL_BGR;
	}

	// create, bind texture and set parameters
	std::shared_ptr<Texture2D> tmpTex = std::shared_ptr<Texture2D>(new Texture2D(GL_TEXTURE_2D, tmpSurface->w, tmpSurface->h, internalFormat, externalFormat, GL_UNSIGNED_BYTE, tmpSurface->pixels));

	tmpTex->bindTexture(GL_TEXTURE0);
	tmpTex->updateMagFilter(GL_LINEAR);
	tmpTex->updateMinFilter(GL_LINEAR);
	tmpTex->updateSWrap(GL_CLAMP_TO_EDGE);
	tmpTex->updateTWrap(GL_CLAMP_TO_EDGE);
	tmpTex->generateMipmap();
	tmpTex->unbindTexture();


	SDL_FreeSurface(tmpSurface); // texture loaded, free the temporary buffer
	return tmpTex;	// return value of texture ID
}