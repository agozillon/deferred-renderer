This is an off split of an Game Engine (start of one anyway) made by me and a few friends with the focus on the Deferred Rendering aspect. 

As that's the part I worked with in particular along with all the rendering components of the engine. Theirs a few other bits and pieces in here like the EntityManager, Controllers and screen classes made by Martin Grant and audio classes made by Ryan Cook.

To run the demo you'll need a couple of libraries placed in a folder called dev in C:/ drive. so C:/dev. All the libraries you'll need 

(plus extra) can be found here https://www.dropbox.com/s/ynoziipbmhaeu10/dev.zip?dl=0 . 

Alternatively you can download them separately:

SDL 2.0.3

GLEW 1.10.0

FMOD 4.44.35

Assimp 3.0

SDL_image 2.0